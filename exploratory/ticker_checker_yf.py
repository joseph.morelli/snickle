import yfinance as yf

# List of tickers to check
tickers = ['GME', 'SCOR', 'SIRI', 'BZFD', 'CYN']


def check_tickers_availability(tickers):
    available_tickers = []
    unavailable_tickers = []

    for ticker in tickers:
        try:
            # Fetch the data
            data = yf.Ticker(ticker).history(period="1d")

            if not data.empty:
                available_tickers.append(ticker)
            else:
                unavailable_tickers.append(ticker)
        except Exception as e:
            unavailable_tickers.append(ticker)

    return available_tickers, unavailable_tickers


available_tickers, unavailable_tickers = check_tickers_availability(tickers)

print("Available Tickers:")
for ticker in available_tickers:
    print(ticker)

print("\nUnavailable Tickers:")
for ticker in unavailable_tickers:
    print(ticker)
