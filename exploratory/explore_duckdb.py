import duckdb
import os

# Get the directory of the current file
current_dir = os.path.dirname(os.path.abspath(__file__))

# Construct the relative path to the DuckDB database
db_path = os.path.join(current_dir, './', 'trading_algo','db', 'duckdb.db')

# Connect to your DuckDB database
con = duckdb.connect(db_path)

# List all tables in the database
tables = con.execute("SHOW TABLES").fetchdf()
print("Tables in the database:")
print(tables)

# Describe a specific table (replace 'your_table_name' with the actual table name)
table_name = 'transaction_data'  # 'transaction_data' and 'models'
description = con.execute(f"DESCRIBE {table_name}").fetchdf()
print(f"\nDescription of {table_name}:")
print(description)

# View the first few rows of a specific table
data = con.execute(f"SELECT * FROM {table_name} LIMIT 10").fetchdf()
print(f"\nData from {table_name}:")
print(data)
