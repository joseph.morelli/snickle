"""
Version 1, created by Joey. First try at orchestration

Orchestrate predict, buy, sell for Snickle

- Run every 1 min for each ticker in input list
- prediction > buy > delayed sell 1 min > store orchestrator record
- There is one orchestrator record per run and includes transaction_buy_fk and transaction_sell_fk

"""
import os
import logging
import json
import pandas as pd

from celery import Celery
from celery.result import AsyncResult  # dont remove

import yaml
from sqlalchemy import create_engine, text

from lib.trading_model import TradingModel
from lib.trade_executor import TradeExecutor

# Configure logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

# Load configuration
config_path = os.path.join(os.getcwd(), "configs", "configs.yaml")
try:
    with open(config_path, "r") as config_file:
        config = yaml.safe_load(config_file)
except Exception as e:
    logging.error(f"Failed to load config file: {e}")
    raise

setup_conf = config["setup"]
methods_conf = config["methods"]


# Initialize the Celery app
app = Celery('trade_orchestrator', broker='redis://redis:6379/0')

# DB URL that docker compose reads and fills in with local IP (localhost)
DATABASE_URL = "postgresql://myuser:mypassword@postgres:5432/mydb"

# Optional Configuration
app.conf.update(
    result_backend='redis://redis:6379/0',
    task_serializer='json',
    result_serializer='json',
    accept_content=['json'],
    timezone='UTC',
    enable_utc=True,
)


@app.task
def sell_process(ticker: str, predict_fk: str, quantity: int = 1, transaction_buy_fk: str = None) -> dict:
    """
    Execute a sell order for a specific stock, intended to be called by the trade_process task.

    Args:
        ticker (str): The stock ticker symbol.
        predict_fk (str): Foreign key for the prediction, linking predict/buy/sell.
        quantity (int, optional): Quantity of stock to sell. Defaults to 1.
        transaction_buy_fk (str, optional): Foreign key of the buy transaction. Defaults to None.

    Returns:
        dict: A dictionary containing the transaction foreign keys for both buy and sell.
    """
    # Create a postgres engine
    engine = create_engine(DATABASE_URL)
    trader = TradeExecutor(setup_conf, methods_conf, engine)
    trade_data = trader.execute_trade('sell', ticker, quantity, predict_fk)

    transaction_sell_fk = trade_data.get('pk')

    engine.dispose()

    return {'transaction_buy_fk': transaction_buy_fk, 'transaction_sell_fk': transaction_sell_fk}


@app.task
def trade_process(ticker: str) -> dict:
    """
    Run the trade process for a given stock ticker:
    1. Make a prediction.
    2. Make a purchase if trade logic is satisfied.
    3. Submit the sell_process task for the purchased stock after 60 seconds.

    Args:
        ticker (str): The stock ticker symbol.

    Returns:
        dict: A dictionary containing the status and transaction foreign keys for both buy and sell.
    """
    # Create a postgres engine in each instance
    engine = create_engine(DATABASE_URL)
    model = TradingModel(setup_config=setup_conf, ticker=ticker, db_conn=engine)
    if model.model is None:
        logging.error('Ticker submitted to process that does not have a model!')
        return {'status': 'model_not_found'}

    prediction_dict = model.make_prediction()

    orchestration_data = {}

    # Implement trade Y/N logic here, simple 0.50 threshold
    if prediction_dict.get("prediction") > 0.50:
        trader = TradeExecutor(setup_conf, methods_conf, engine)
        trade_data = trader.execute_trade('buy', model.ticker, 1, prediction_dict.get('pk'))
        transaction_buy_fk = trade_data.get('pk')

        # Execute sell process and store all data together
        sell_result = sell_process(
            model.ticker, prediction_dict.get('pk'), transaction_buy_fk=transaction_buy_fk
        )
        transaction_sell_fk = sell_result['transaction_sell_fk']

        # Store the orchestration data in the same task
        orchestration_data = {
            'ticker': ticker,
            'prediction_fk': prediction_dict.get('pk'),
            'transaction_buy_fk': transaction_buy_fk,
            'transaction_sell_fk': transaction_sell_fk,
            'status': 'trade_process_completed'
        }
        store_trade_process_result(orchestration_data)

    # Close connection before ending to eliminate connection leak
    engine.dispose()

    return orchestration_data


def store_trade_process_result(orchestration_data: dict) -> None:
    """Store the result of trade_process task in the database.

    Args:
        orchestration_data (dict): A dictionary containing orchestration data.
    """
    # Convert orchestration data dictionary to DataFrame
    trade_data_df = pd.DataFrame([orchestration_data])

    # Store the data in the PostgreSQL database
    engine = create_engine(DATABASE_URL)
    trade_data_df.to_sql(
        name='orchestrator_events',
        con=engine,
        schema='transactions',
        if_exists='append',
        index=False,
    )
    engine.dispose()


@app.task
def launch_trade_processes() -> None:
    """Launch trade processes and store the result status.

    """
    engine = create_engine(DATABASE_URL)

    ticker_list = pd.read_sql("Select ticker from orchestrator.tickers where active=True", con=engine)
    logging.info(f'ticker list: {ticker_list}')
    for ticker in ticker_list['ticker'].tolist():
        # Launch the trade_process task without using get()
        trade_process.delay(ticker)
    engine.dispose()

    return


# Schedule configuration
# Defines entry point for trade task, runs every 60 seconds
app.conf.beat_schedule = {
    'trade-every-60-seconds': {
        'task': 'trade_orchestrator.launch_trade_processes',
        'schedule': 60.0,
    },
}


if __name__ == '__main__':
    app.start()