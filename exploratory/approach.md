## TODO
- pandas df retain state even if manipulated in a funct. do data.copy() everywhere
- containerize
- retrain pull from config
- add features and pull data based on config


## Next steps
*Commander*
- central interface to program each microservice
  - Train, schedule, spin up, run, data reset, etc.

*Meth*
- consider buy, then auto sell (instead of sell on prediction)
- Microservices, one process per ticker
- Run every 5 min for 3 hours during the day
- Quantity to buy/sell based on model probability (e.g., higher probability = higher quantity)
- Avoid volatility of open
- Run during lower volume, to reduce volatility that comes with high-volume and a high media visibility index
    - Potentially a secondary model that determines low volatility (i.e., when and when not to conduct trading)


*Processes*
- Retrain (daily)


### *Reporting*
- Daily report (gains, close-open, trades)
  - balance_change = float(account.equity) - float(account.last_equity)  # Check our current balance vs. our balance at the last market close
  - Alpaca get_position()
  - 


### *Model*
- Lag features


*Backlog*
- automate deployment to gcloud
- separate environments
- microservices, repeat deployment for each ticker


## NOTE

## Alpaca api
- search for US equities
search_params = GetAssetsRequest(asset_class=AssetClass.US_EQUITY)
assets = trading_client.get_all_assets(search_params)

- get the latest status of your position with a ticker
api.get_position('AAPL')


## GCP
gcloud projects add-iam-policy-binding stellar-perigee-428320-c2 `
    --member="serviceAccount:snickle-sa@stellar-perigee-428320-c2.iam.gserviceaccount.com" `
    --role="roles/secretmanager.secretAccessor"

gcloud projects get-iam-policy stellar-perigee-428320-c2

gcloud auth login
