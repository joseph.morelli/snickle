"""
Version 2, created by Joe
    Unnecessary class setup (can remove Task and base=TradeTask without issue)
    Uses async but we're moving away from that in version 3
    Everything from config

"""

import os
import time
import requests
from celery import Celery, Task
from datetime import datetime, timedelta, timezone
import pytz
import yaml
import logging
import pandas as pd
from sqlalchemy import create_engine

# Configure logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

# Load configuration
config_path = os.path.join(os.getcwd(), "configs", "configs.yaml")
try:
    with open(config_path, "r") as config_file:
        config = yaml.safe_load(config_file)
except Exception as e:
    logging.error(f"Failed to load config file: {e}")
    raise

methods_conf = config["methods"]
orche_conf = config["orchestration"]

# Setup
DATABASE_URL = "postgresql://myuser:mypassword@postgres:5432/mydb"
BASE_URL = "http://flask:8080"  # flask app url
app = Celery('trade_orchestrator', broker='redis://redis:6379/0')

# Optional Configuration
app.conf.update(
    result_backend='redis://redis:6379/0',
    task_serializer='json',
    result_serializer='json',
    accept_content=['json'],
    timezone='UTC',
    enable_utc=True,
)

# Schedule configuration
# Defines entry point for trade task, runs every XX seconds
app.conf.beat_schedule = {
    'run_trading_task_every_X_minutes': {
        'task': 'trade_orchestrator.run_trading_task',
        'schedule': orche_conf['run_cadence'] * 60,
    },
}


class TradingTask(Task):
    abstract = True

    def __init__(self):
        self.timezone_str = orche_conf['timezone']
        self.sched_start_time = self._convert_to_utc(orche_conf['sched_start_time'])
        self.sched_end_time = self._convert_to_utc(orche_conf['sched_end_time'])
        self.tickers = orche_conf['tickers']
        self.train_days = orche_conf['train_days']

        self.sell_delay = orche_conf['sell_delay']
        self.run_frequency = orche_conf['run_cadence']
        self.force_run = orche_conf['force_run']
        self.trading_active = False

    def _convert_to_utc(self, time_str):
        # Parse the time string into a time object
        local_time = datetime.strptime(time_str, '%H:%M:%S').time()

        # Get the current date and combine it with the parsed time
        local_datetime = datetime.combine(datetime.now().date(), local_time)

        # Localize the datetime to the specified timezone
        local_tz = pytz.timezone(self.timezone_str)
        local_datetime = local_tz.localize(local_datetime)

        # Convert the localized time to UTC
        utc_datetime = local_datetime.astimezone(pytz.utc)

        # Return the UTC time
        return utc_datetime.time()

    def _retrain_all_models(self):
        for ticker in self.tickers:
            self.train_model(ticker)

    def _trade_logic(self, prediction):
        quantity = 10
        signal = "buy" if prediction > orche_conf["buy_threshold"] else "hold"

        return quantity, signal

    def is_within_schedule(self):
        now_utc = datetime.now(timezone.utc).time()
        return self.sched_start_time <= now_utc <= self.sched_end_time

    def on_startup(self):
        self._retrain_all_models()

    def train_model(self, ticker):
        end_date = datetime.today()
        start_date = end_date - timedelta(days=self.train_days)
        start_date_str = start_date.strftime('%Y-%m-%d')
        end_date_str = end_date.strftime('%Y-%m-%d')

        data = {
            "ticker": ticker,
            "start_date": start_date_str,
            "end_date": end_date_str,
            "orchestrator_fk": 1  # TODO Create orchestrator key for startup
        }
        response = requests.post(f"{BASE_URL}/model/train", json=data)
        logging.info(f"Training response for {ticker}: {response.json()}")

    def execute_tasks(self, ticker):
        orchestrator_pk = str(int(time.time())) + ticker

        log_orchestration_run(orchestrator_pk, ticker)

        predict_data = {
            "ticker": ticker,
            "orchestrator_fk": orchestrator_pk
        }
        predict_response = requests.post(f"{BASE_URL}/model/predict", json=predict_data)
        prediction_load = predict_response.json()
        prediction = prediction_load.get("prediction", 0)

        quantity, signal = self._trade_logic(prediction)
        logging.info(f"Prediction and status on buy task for {ticker}. pred:{prediction}, status:{signal}")

        # TODO: Add logic to sell as part of execute_tasks
        if signal == "buy":
            buy_data = {
                "signal": "buy",
                "ticker": ticker,
                "qty": quantity,
                "predict_fk": prediction_load.get("pk"),
                "orchestrate_fk": orchestrator_pk
            }
            trade_response = requests.post(f"{BASE_URL}/trader/make-trade", json=buy_data)
            trade = trade_response.json()
            logging.info(f"Trade response for {ticker}: {trade_response.json()}")

            if trade.get("status", 0).lower() == "filled":
                sell_data = {
                    "signal": 'sell',
                    "ticker": ticker,
                    "qty": -1,  # sell all owned shares
                    "predict_fk": prediction_load.get("pk"),
                    "orchestrate_fk": orchestrator_pk
                }
                queue_sell_task.apply_async(args=[sell_data, predict_data, ticker], countdown=self.sell_delay * 60)


@app.on_after_configure.connect
def kickoff_actions(sender, **kwargs):
    # Execute startup tasks
    trading_task = TradingTask()
    trading_task.on_startup()


@app.task(base=TradingTask)
def run_trading_task():
    task = run_trading_task._get_current_object()
    if task.force_run or task.is_within_schedule():
        for ticker in task.tickers:
            task.execute_tasks(ticker)


@app.task
def queue_sell_task(sell_data, predict_data, ticker):

    predict_response = requests.post(f"{BASE_URL}/model/predict", json=predict_data)

    prediction = predict_response.json()
    logging.info(f"Prediction check on sell. Response for {ticker}: {prediction}")

    if prediction.get("prediction", 0) < orche_conf["sell_threshold"]:
        # Clean up open orders in preparation for sell
        cancel_data = {
            "ticker": ticker,
            "order_types": None,
        }
        cancel_response = requests.post(f"{BASE_URL}/trader/cancel-open", json=cancel_data)
        logging.info(f"Cancelled opened orders for {ticker}")

        response = requests.post(f"{BASE_URL}/trader/make-trade", json=sell_data)
        logging.info(f"Queued Sell response: {response.json()}")
    else:
        logging.info(f"Sell skipped. Holding")


def log_orchestration_run(orchestrator_pk: str, ticker: str) -> None:
    execution_time = datetime.now(timezone.utc)

    orchestration_data = {
        'pk': orchestrator_pk,
        "date": execution_time.date(),
        "execution_time": execution_time,
        'ticker': ticker,
    }
    # Convert orchestration data dictionary to DataFrame
    trade_data_df = pd.DataFrame([orchestration_data])

    # Store the data in the PostgreSQL database
    engine = create_engine(DATABASE_URL)

    trade_data_df.to_sql(
        name='orchestrator_events',
        con=engine,
        schema='transactions',
        if_exists='append',
        index=False,
    )
    engine.dispose()


if __name__ == '__main__':
    app.start()
