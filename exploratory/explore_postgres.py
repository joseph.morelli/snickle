import os
import pandas as pd
from sqlalchemy import create_engine, inspect, text

# Configuration for database connection
DATABASE_URL = "postgresql://myuser:mypassword@postgres:5432/mydb"

# Create a Postgres engine
engine = create_engine(DATABASE_URL)

def list_schemas(engine):
    """List all schemas in the PostgreSQL database."""
    with engine.connect() as conn:
        result = conn.execute(text("SELECT schema_name FROM information_schema.schemata"))
        schemas = [row[0] for row in result]
    return schemas

def list_tables(engine, schema):
    """List all tables in a specific schema."""
    inspector = inspect(engine)
    tables = inspector.get_table_names(schema=schema)
    return tables

def preview_table(engine, schema, table, limit=5):
    """Preview data from a specific table."""
    query = f"SELECT * FROM {schema}.{table} LIMIT {limit}"
    df = pd.read_sql(query, engine)
    return df

def get_table_columns(engine, schema, table):
    """Get columns of a specific table."""
    inspector = inspect(engine)
    columns = inspector.get_columns(table, schema=schema)
    return columns

if __name__ == "__main__":
    print("Connecting to the PostgreSQL database...\n")

    # List all schemas
    schemas = list_schemas(engine)
    print("Schemas available in the database:")
    for schema in schemas:
        print(f" - {schema}")

    # Explore a specific schema
    schema_name = input("\nEnter a schema to explore: ")
    if schema_name not in schemas:
        print(f"Schema '{schema_name}' not found.")
    else:
        # List tables in the selected schema
        tables = list_tables(engine, schema_name)
        print(f"\nTables in schema '{schema_name}':")
        for table in tables:
            print(f" - {table}")

        # Explore a specific table
        table_name = input("\nEnter a table to preview: ")
        if table_name not in tables:
            print(f"Table '{table_name}' not found in schema '{schema_name}'.")
        else:
            # Preview table data
            print(f"\nPreviewing data from {schema_name}.{table_name}...")
            table_preview = preview_table(engine, schema_name, table_name)
            print(table_preview)

            # Display columns of the table
            columns = get_table_columns(engine, schema_name, table_name)
            print(f"\nColumns in {schema_name}.{table_name}:")
            for column in columns:
                print(f" - {column['name']} ({column['type']})")

    print("\nExploration complete.")
