import tinstalime
from datetime import datetime, timedelta


def should_buy(prediction):
    # Define your buy logic here
    return prediction > 0.5


def should_sell(prediction):
    # Define your sell logic here
    return prediction < 0.5


while True:
    # Get the latest stock data
    # Prepare features
    # Get prediction from model
    response = requests.post("http://your-flask-app-url/predict", json=features)
    prediction = response.json()["prediction"]

    # Trading logic
    if should_buy(prediction):
        api.submit_order(
            symbol="AAPL", qty=1, side="buy", type="market", time_in_force="gtc"
        )
    elif should_sell(prediction):
        api.submit_order(
            symbol="AAPL", qty=1, side="sell", type="market", time_in_force="gtc"
        )

    # Sleep until the next interval
    time.sleep(60)


# gcloud IAM shit
import os


def check_google_application_credentials():
    credentials_path = os.getenv("GOOGLE_APPLICATION_CREDENTIALS")
    if credentials_path:
        print(f"GOOGLE_APPLICATION_CREDENTIALS is set to: {credentials_path}")
        if os.path.exists(credentials_path):
            print("The file exists and is accessible.")
        else:
            print("The file does not exist or is not accessible.")
    else:
        print("GOOGLE_APPLICATION_CREDENTIALS is not set.")


if __name__ == "__main__":
    check_google_application_credentials()


try:
    with open(os.getenv("GOOGLE_APPLICATION_CREDENTIALS"), "r") as f:
        print("File opened successfully.")
except Exception as e:
    print(f"Error: {e}")

from google.cloud import secretmanager


def get_secret(secret_id, version_id="latest"):
    client = secretmanager.SecretManagerServiceClient()
    name = (
        f"projects/stellar-perigee-428320-c2/secrets/{secret_id}/versions/{version_id}"
    )
    response = client.access_secret_version(request={"name": name})
    return response.payload.data.decode("UTF-8")


# Replace YOUR_PROJECT_ID with your actual project ID
api_key = get_secret("ALPACA_API_KEY_PAPER")
api_secret = get_secret("ALPACA_API_SECRET_PAPER")
base_url = get_secret("ALPACA_BASE_URL")

print(api_key)
print(api_secret)
print(base_url)
