from celery import Celery
from celery.schedules import crontab
import logging
import pickle  # celery uses JSON serialization, which no work on objects. hacky pickle solution for now
from trading_algo.lib.trade_orchestrator_support.functions import (
    instantiate_ticker_operators,
    get_new_york_time,
)
from trading_algo.configs.load_config import load_config


# Configure logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

# Load configuration
config = load_config()
setup_conf = config["setup"]
methods_conf = config["methods"]
orche_conf = config["orchestration"]
orche_conf["DATABASE_URL"] = setup_conf["DATABASE_URL"]
orche_conf["BASE_URL"] = setup_conf["BASE_URL"]

# Setup
app = Celery("trade_orchestrator", broker="redis://redis:6379/0", include=[
    "trading_algo.trade_orchestrator"
])

# Optional Configuration
app.conf.update(
    result_backend="redis://redis:6379/0",
    task_serializer="json",
    result_serializer="json",
    accept_content=["json"],
    timezone="America/New_York",  # UTC here was overwriting the nowfun
    enable_utc=False,
)

# Schedule configuration
# Defines entry point for trade task, runs every XX seconds

# Assuming config set to 10:30a - 3:57p see the below behavior
# - run_trading_task_every_X_minutes runs from 10a to 4p, last run at 3:55p
# - run_train_every_morning runs at 9:30a
# - run-sell_every_close runs at 3:59p
app.conf.beat_schedule = {
    "run_trading_task_every_X_minutes": {
        "task": "trade_orchestrator.run_trading_task",
        "schedule": crontab(
            minute=f"*/{orche_conf['run_cadence']}",
            hour=f'{orche_conf["sched_start_time_hour"]}-{orche_conf["sched_end_time_hour"]}',
            day_of_week="1-5",
            # nowfun=get_new_york_time,
        ),
    },
    "run_train_every_morning": {
        "task": "trade_orchestrator.run_train_task",
        "schedule": crontab(
            hour=orche_conf["sched_start_time_hour"] - 1,
            minute=orche_conf["sched_start_time_minute"],
            day_of_week="1-5",
            # nowfun=get_new_york_time,
        ),
    },
    "run_sell_every_close": {
        "task": "trade_orchestrator.end_of_day_cleanup_task",
        "schedule": crontab(
            hour=orche_conf["sched_end_time_hour"],
            minute=orche_conf["sched_end_time_minute"],
            day_of_week="1-5",
            # nowfun=get_new_york_time,
        ),
    },
}


""" 
BLOCK
END OF DAY CLEANUP
"""


@app.task(name='trade_orchestrator.end_of_day_cleanup_task')
def end_of_day_cleanup_task():
    # Execute a final sell command for all tickers

    ticker_operators = instantiate_ticker_operators(orche_conf, "sched_eod_cleanup")

    for ticker_operator in ticker_operators:
        cleanup.apply_async(args=[pickle.dumps(ticker_operator)])


@app.task(name='trade_orchestrator.cleanup')
def cleanup(pickled_operator: bytes):
    ticker_operator = pickle.loads(pickled_operator)
    ticker_operator.end_of_day_cleanup()
    logging.info(f"End of day cleanup executed: {ticker_operator.ticker}")


"""  
BLOCK
RUN TRADING TASK
"""


@app.task(name='trade_orchestrator.run_trading_task')
def run_trading_task():

    ticker_operators = instantiate_ticker_operators(orche_conf, "sched_trade")

    for ticker_operator in ticker_operators:
        execute_trade.apply_async(args=[pickle.dumps(ticker_operator)])


# probably a shorter way to do this but being explicit for now
@app.task(name='trade_orchestrator.execute_trade')
def execute_trade(pickled_operator: bytes):
    ticker_operator = pickle.loads(pickled_operator)
    ticker_operator.execute()


""" 
BLOCK 
TRAIN MODELS
"""


@app.task(name='trade_orchestrator.run_train_task')
def run_train_task():
    """
    starts one async job per ticker to take advantage of multiple workers
    """
    ticker_operators = instantiate_ticker_operators(orche_conf, "sched_train")
    for ticker_operator in ticker_operators:
        train_model.apply_async(args=[pickle.dumps(ticker_operator)])


@app.task(name='trade_orchestrator.train_model')
def train_model(pickled_operator: bytes):
    """
    individual train job for each ticker, for training individual models
    """
    # This instantiation can be switched out for anything we move to

    ticker_operator = pickle.loads(pickled_operator)
    ticker_operator.train_model()


@app.on_after_configure.connect
def kickoff_task(sender, **kwargs):

    if orche_conf["train_on_startup"]:
        ticker_operators = instantiate_ticker_operators(orche_conf, "startup_train")
        for ticker_operator in ticker_operators:
            train_model.apply_async(args=[pickle.dumps(ticker_operator)])


if __name__ == "__main__":
    app.start()
