"""
Simple script to pull raw stock data for simulated runs.

- Must be run from this directory
- Pulls the number of days of data and tickers of interest from configs["simulation"]
"""
import os

from trading_algo.lib.data_pull import get_data_in_chunks
import pandas as pd
from datetime import datetime, timedelta
from trading_algo.configs.load_config import load_config

config = load_config()

ticker_list = config["simulation"]["ticker_list"]

end_date = datetime.today()
start_date = end_date - timedelta(days=config["simulation"]["train_days"])
data = {}
data["start_date"] = start_date.strftime("%Y-%m-%d")
data["end_date"] = end_date.strftime("%Y-%m-%d")
start_date = datetime.strptime(data["start_date"], "%Y-%m-%d")
end_date = datetime.strptime(data["end_date"], "%Y-%m-%d")

for ticker in ticker_list:

    raw_data = get_data_in_chunks(ticker, start_date, end_date, run_type="train")
    raw_data["ticker"] = ticker
    # Write original file if not exists
    if not os.path.exists("raw_stock_data.parquet"):
        raw_data.to_parquet("raw_stock_data.parquet", index=False)
    # Read original parquet
    original_raw_data = pd.read_parquet("raw_stock_data.parquet")
    # Concat
    combined_raw_data = pd.concat([original_raw_data, raw_data], ignore_index=True)
    # Dedupe
    combined_raw_data = combined_raw_data.drop_duplicates()
    # Write back to parquet
    combined_raw_data.to_parquet("raw_stock_data.parquet", index=False)
