"""
Helper function for pulling in simulated run data to main.py and displayed on the front end


DOES NOT NEED TO BE RUN DIRECTLY
"""

import os
import logging
import pandas as pd


def load_model_simulation_data():
    """
    Access local simulation_results.parquet. Used within main.py

    Returns:
        - simulation_results_data (parquet of simulated runs)
    """
    model_simulation_data_dir = os.path.dirname(os.path.realpath(__file__))

    # Load model simulation data
    model_simulation_data_path = os.path.join(
        model_simulation_data_dir, "simulation_results.parquet"
    )

    try:
        simulation_results_data = pd.read_parquet(model_simulation_data_path)
        return simulation_results_data
    except Exception as e:
        logging.error(f"Failed to load model simulation parquet: {e}")
        raise


if __name__ == "__main__":
    load_model_simulation_data()
