"""
Script to get local raw_stock_data.parquet

- Must be run from this directory
- Pulls in configurations from configs["simulation"] and {model_specific_config}["labels"]
- Must specify and import the model_strategy {ModelStrat} within this script
"""
import os
import time
import numpy as np
import pandas as pd
from datetime import timedelta
from sklearn.metrics import accuracy_score, precision_score, recall_score

from trading_algo.lib.model_strategy import MODEL_STRATEGIES
from trading_algo.lib.label_strategy import LABEL_STRATEGIES
from trading_algo.configs.load_config import load_config, load_model_config


#############################
# Constants & Configuration #
#############################

# Load configs
config = load_config()

# Configs from main config file
simulation_config = config["simulation"]
TRAIN_WINDOW_SIZE = simulation_config["train_window_size"]
TICKER_LIST = simulation_config["ticker_list"]
BUY_THRESHOLD = simulation_config["buy_threshold"]
SELL_THRESHOLD = simulation_config["sell_threshold"]
RUN_CADENCE = simulation_config["run_cadence"]

# Model-specific configs
model_strategy_config = config["model_strategy"][0]
model_strategy_name = model_strategy_config["name"]
model_strategy_args = model_strategy_config.get("args", {})
model_config_file = model_strategy_args["config"]
model_specific_config = load_model_config(model_config_file)

ModelStrat = MODEL_STRATEGIES.get(model_strategy_name)
if not ModelStrat:
    raise ValueError(
        f"Unknown model strategy: {ModelStrat}. "
        f"Please add it to MODEL_STRATEGIES dict."
    )

label_strategy_config = model_specific_config["label_strategy"][0]
label_strategy_name = label_strategy_config["name"]
LABEL_STRATEGY_ARGS = label_strategy_config.get("args", {})
LabelStrat = LABEL_STRATEGIES.get(label_strategy_name)

if not LabelStrat:
    raise ValueError(
        f"Unknown label strategy: {label_strategy_name}. "
        f"Please add it to LABEL_STRATEGIES dict."
    )


#######################
# Utility/Helper Func #
#######################
def get_prediction_percentiles(predictions: np.ndarray) -> list:
    """
    Compute specific percentiles for a list of predictions.

    :param predictions: Array or list of prediction probabilities
    :return: List of percentiles [10th, 25th, 50th, 75th, 90th]
    """
    sorted_predictions = sorted(predictions)
    percentiles = [10, 25, 50, 75, 90]
    result = np.percentile(sorted_predictions, percentiles)
    return result.tolist()


def read_stock_data(file_path: str) -> pd.DataFrame:
    """
    Read the parquet file containing raw stock data.

    :param file_path: Path to the parquet file
    :return: DataFrame with the stock data
    """
    if not os.path.exists(file_path):
        raise FileNotFoundError(f"Could not find the file: {file_path}")

    df = pd.read_parquet(file_path)
    df["datetime"] = pd.to_datetime(df["unixtime"], unit="s")
    return df


def store_results(results_df: pd.DataFrame, output_file: str = "simulation_results.parquet") -> None:
    """
    Append or create a parquet file with simulation results.

    :param results_df: DataFrame of results to be stored
    :param output_file: File path for parquet output
    """
    if not os.path.exists(output_file):
        # If parquet file doesn't exist, write it from scratch
        results_df.to_parquet(output_file, index=False)
    else:
        # Otherwise, read existing, append, then write back
        original_df = pd.read_parquet(output_file)
        combined_df = pd.concat([original_df, results_df], ignore_index=True)
        combined_df.to_parquet(output_file, index=False)


#############################
# Main Simulation Function  #
#############################
def run_predictions(ticker_list, train_window_size, run_cadence, ModelStrat, LabelStrat, label_strategy_args):
    """
    Run model simulations given loaded configs

    1) Pull in raw stock data
    2) Create sliding train and test windows:
        - Train window from config
        - Test window is the day after train window
    3) Iterate over those windows, shifting forward in time by one day:
        - Run predictions for every row
        - Calculate labels
        - Simulate model run for test window
        - Calculate success metrics (accuracy, precision, recall)
        - Calculate simulated PnL
        - Record all metrics to a parquet file

    Args:
        ticker_list: List of tickers to simulate
        train_window_size: Window size (in days) for model training
        ModelStrat: Class implementing the model strategy

    Returns:
        None. Writes outputs to "simulation_results.parquet"
    """
    # Attempt to read your stock data
    try:
        all_data = read_stock_data("raw_stock_data.parquet")
    except FileNotFoundError as e:
        print(f"File not found error: {e}")
        return
    except Exception as e:
        print(f"Unexpected error reading stock data: {e}")
        return

    # Unique simulation "primary key" for grouping these runs
    pk = str(time.time()) + "SIMULATION"

    for ticker in ticker_list:
        # Slice data for this ticker only
        ticker_data = all_data.query(f'ticker == "{ticker}"')

        if ticker_data.empty:
            print(f"[WARNING] No data available for ticker {ticker}. Skipping.")
            continue

        # Ensure the data is sorted by datetime
        ticker_data = ticker_data.sort_values(by="datetime")

        # Drop ticker column and set min/max dates to the start of the day (i.e. midnight)
        ticker_data = ticker_data.drop(columns=["ticker"])
        max_date = ticker_data["datetime"].max().replace(hour=0, minute=0, second=0)
        min_date = ticker_data["datetime"].min().replace(hour=0, minute=0, second=0)

        # Initialize sliding window boundaries
        train_start = min_date
        train_end = min_date + timedelta(days=train_window_size)
        test_end = train_end + timedelta(days=1)

        # Slide windows until test_end surpasses max_date
        while test_end <= max_date:
            try:
                # Separate train/test windows
                train_data_window = ticker_data.query(
                    f'datetime >= "{train_start}" & datetime <= "{train_end}"'
                )
                test_data_window = ticker_data.query(
                    f'datetime > "{train_end}" & datetime <= "{test_end}"'
                )

                # Check for empty train/test
                if train_data_window.empty:
                    print(f"[WARNING] No data in TRAIN window ({ticker}): {train_start} to {train_end}")
                    # Shift window by one day and continue
                    train_start += timedelta(days=1)
                    train_end += timedelta(days=1)
                    test_end += timedelta(days=1)
                    continue

                if test_data_window.empty:
                    print(f"[WARNING] No data in TEST window ({ticker}): {train_end} to {test_end}")
                    # Try next day for test
                    test_end += timedelta(days=1)
                    continue

                # Prepare data for training/testing (drop datetime for model)
                train_data_window_model = train_data_window.drop(columns=["datetime"])
                test_data_window_model = test_data_window.drop(columns=["datetime"])

                # Instantiate model and label
                model_strategy = ModelStrat(ticker)
                label_strategy = LabelStrat(**label_strategy_args)

                # Run training
                fk = str(time.time()) + ticker + "SIMULATION"
                model_strategy.run_train_pipeline(train_data_window_model, fk)

                # Get predictions
                test_data_window_model["prediction_object"] = model_strategy.run_simulation_pipeline(
                    test_data_window_model, fk
                )

                # Extract probability from the prediction object
                test_data_window["probability"] = test_data_window_model["prediction_object"].apply(
                    lambda x: x.prediction
                )

                # Compute labels (binary: 1 if Close after `label_cadence` > current Close)
                _, test_data_window["labels"] = label_strategy.generate_labels(test_data_window, filter_nulls=False)

                # Determine predicted labels from probability
                test_data_window["predicted_labels"] = (test_data_window["probability"] > 0.5).astype(int)

                # Drop rows with missing relevant columns (due to shifting)
                test_data_window.dropna(subset=["labels", "probability", "predicted_labels"], inplace=True)

                # Convert labels back to int if needed
                test_data_window["labels"] = test_data_window["labels"].astype(int)

                # Simulate trading
                stock_owned = 0
                buy_prices = []
                total_pnl = 0.0
                last_trade_time = None

                for index, row in test_data_window.iterrows():
                    current_time = row["datetime"]

                    if (last_trade_time is None) or ((current_time - last_trade_time).total_seconds() >= run_cadence * 60):
                        last_trade_time = current_time  # Update last trade time

                        probability = row["probability"]
                        close_price = row["Close"]

                        # Buy condition: Probability > BUY_THRESHOLD
                        if probability > BUY_THRESHOLD:
                            stock_owned += 1
                            buy_prices.append(close_price)  # Record the buy price
                            print(f"[INFO] Bought 1 stock of {ticker} at {close_price} on {current_time}")

                        # Sell condition: Probability < SELL_THRESHOLD
                        elif probability < SELL_THRESHOLD and stock_owned > 0:
                            # Calculate PnL for all owned stocks
                            pnl = sum(close_price - price for price in buy_prices)
                            total_pnl += pnl
                            print(f"[INFO] Sold all stocks of {ticker} at {close_price} on {current_time}, PnL: {pnl}")

                            # Reset stock tracking
                            stock_owned = 0
                            buy_prices = []

                # End-of-window: Sell any remaining stocks
                if stock_owned > 0:
                    final_sell_price = test_data_window.iloc[-1]["Close"]
                    pnl = sum(final_sell_price - price for price in buy_prices)
                    total_pnl += pnl
                    print(f"[INFO] Final sell for {ticker} at {final_sell_price}, PnL: {pnl}")

                    # Reset stock tracking
                    stock_owned = 0
                    buy_prices = []

                # Compute metrics
                accuracy = accuracy_score(test_data_window["labels"], test_data_window["predicted_labels"])
                precision = precision_score(test_data_window["labels"], test_data_window["predicted_labels"])
                recall = recall_score(test_data_window["labels"], test_data_window["predicted_labels"])
                positive_percent = test_data_window["predicted_labels"].sum() / len(test_data_window)
                actual_positive_percent = test_data_window["labels"].sum() / len(test_data_window)
                percentiles = get_prediction_percentiles(test_data_window["probability"].values)

                # Print summary
                print(f"\n=== {ticker} Simulation Results ===")
                print(f"Train Window : {train_start} to {train_end}")
                print(f"Test Window  : {train_end} to {test_end}")
                print(f"PnL          : {total_pnl}")
                print(f"Accuracy     : {accuracy}")
                print(f"Precision    : {precision}")
                print(f"Recall       : {recall}")
                print(f"Probability Percentiles: {percentiles}\n")

                # Prepare results for storage
                results_df = pd.DataFrame({
                    "pk": [pk],
                    "ticker": [ticker],
                    "train_start_date": [train_start],
                    "train_end_date": [train_end],
                    "test_date": [test_end],
                    "accuracy": [accuracy],
                    "precision": [precision],
                    "recall": [recall],
                    "positive_percent": [positive_percent],
                    "actual_positive_percent": [actual_positive_percent],
                    "percentiles": [percentiles],
                    "orchestrator_fk": [fk],
                    "calculated_features": [model_strategy.model_config["calculated_features"]],
                    "feature_params": [model_strategy.model_config["feature_params"]],
                    "model_type": [model_strategy.__class__.__name__],
                    "total_pnl": [total_pnl],
                })

                # Store the results in a parquet file
                store_results(results_df, "simulation_results.parquet")

            except Exception as e:
                # Catch-all for any unexpected issues in this window
                print(f"[ERROR] Exception occurred for {ticker} window ({train_start} to {test_end}): {e}")

            finally:
                # Always shift window by one day for next iteration
                train_start += timedelta(days=1)
                train_end += timedelta(days=1)
                test_end += timedelta(days=1)


########################
# Entry Point #
########################

if __name__ == "__main__":
    run_predictions(TICKER_LIST, TRAIN_WINDOW_SIZE, RUN_CADENCE, ModelStrat, LabelStrat, LABEL_STRATEGY_ARGS)
