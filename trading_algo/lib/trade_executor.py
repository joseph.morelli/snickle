import alpaca_trade_api as tradeapi
from google.cloud import secretmanager
import time
import logging
import json
import pandas as pd
from datetime import datetime, timezone
from typing import Any, Dict, List, Tuple
from sqlalchemy import engine

# Configure logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def get_secret(secret_id: str, project_id: str, version_id: str = "latest") -> str:
    """
    Fetches the secret value from Google Secret Manager.
    """
    client = secretmanager.SecretManagerServiceClient()
    name = f"projects/{project_id}/secrets/{secret_id}/versions/{version_id}"
    try:
        response = client.access_secret_version(request={"name": name})
        return response.payload.data.decode("UTF-8")
    except Exception as e:
        logging.error(f"Failed to access secret: {e}")
        raise


class Order:
    """
    Represents a trade order with relevant details.
    """

    def __init__(
        self, id: str, status: str, symbol: str, raw_data: Dict[str, Any] = None
    ):
        self.id = id
        self.status = status
        self.symbol = symbol
        self.raw_data = raw_data or {}

    def to_dict(self) -> Dict[str, Any]:
        return {
            "id": self.id,
            "status": self.status,
            "symbol": self.symbol,
            "raw_data": self.raw_data,
        }


# TODO: Add open order check to cancel_order_status method
class TradeExecutor:
    """
    Initializes the TradeExecutor with API credentials, configuration settings, and empty market/order status.

    At the top of each external method, update market/order status and ticker
    At the bottom of each external method, clear market/order status and ticker

    Args:
        setup_config (Dict): Configuration settings for API keys and project setup.
        methods_config (Dict): Configuration settings for trading methods and parameters.
        mode (str): mode for run - nonprod pulls alpaca api info from setup_config,
            prod pulls from google secrets
    """

    def __init__(
        self,
        setup_config: Dict,
        methods_config: Dict,
        db_conn: engine,
        mode: str = "nonprod",
    ):
        alpaca_secret_name = setup_config["alpaca"]["secret_name"]
        if mode == "nonprod":
            api_key_dict = json.loads(setup_config["alpaca"]["secret_value"])
        elif mode == "prod":
            project_id = setup_config["project_id"]
            api_key_dict = json.loads(get_secret(alpaca_secret_name, project_id))
        else:
            raise ValueError(
                f"Unknown value supplied for run mode: {mode}, must be either 'nonprod' or 'prod'"
            )
        self.api = tradeapi.REST(
            api_key_dict["API_KEY"],
            api_key_dict["API_SECRET"],
            base_url=api_key_dict["PAPER_ENDPOINT"],
        )
        self.engine = db_conn

        self.open_order_wait = methods_config["open_order_wait"]
        self.take_profit_multiplier_sell = methods_config["take_profit_multiplier_sell"]
        self.stop_loss_multiplier_sell = methods_config["stop_loss_multiplier_sell"]
        self.take_profit_multiplier_buy = methods_config["take_profit_multiplier_buy"]
        self.stop_loss_multiplier_buy = methods_config["stop_loss_multiplier_buy"]
        self.ticker = None
        self.market_status = {}
        self.order_status = {}
        self.position_status = {}

    def cancel_open_orders(
        self, ticker: str, order_types: List[str] = None, retain_attributes: bool = False
    ) -> Dict[str, Any]:
        """
        Cancels open orders for a given ticker and specific order types.

        Args:
            ticker (str): The ticker symbol for which to cancel orders.
            order_types (List[str], optional): The types of orders to cancel (e.g., ["limit", "stop"]).
                Defaults to None, which cancels all open orders.
            retain_attributes (bool): Only retain attributes when specified, like in _submit_order.
                There the attributes must remain to complete execute_trade.
                At that point attributes are cleared through execute_trade

        Returns:
            Dict[str, Any]: A summary of canceled orders.
        """
        try:
            self.ticker = ticker
            self._update_order_status()
            self._update_position_status()

            open_orders = self.order_status["open_orders"]
            canceled_orders = []

            for order in open_orders:
                if order.symbol == self.ticker and (
                    order_types is None or order.order_type in order_types
                ):
                    self.api.cancel_order(order.id)

                    order_details = self.api.get_order(order.id)

                    canceled_orders.append(
                        Order(
                            id=order_details.id,
                            status=order_details.status,
                            symbol=order_details.symbol,
                        )
                    )

            logging.info(
                f"Canceled orders for {self.ticker}: {[order.to_dict() for order in canceled_orders]}"
            )

            return {
                "status": "Canceled",
                "canceled_orders": [order.to_dict() for order in canceled_orders],
            }
        except tradeapi.rest.APIError as e:
            logging.error(
                f"API error while canceling open orders for {self.ticker}: {e}"
            )
            return {"status": "Failed", "error": str(e)}
        except Exception as e:
            logging.error(
                f"Unexpected error while canceling open orders for {self.ticker}: {e}"
            )
            return {"status": "Failed", "error": str(e)}
        finally:
            if not retain_attributes:
                self.ticker = None
                self.market_status = {}
                self.order_status = {}
                self.position_status = {}

    def execute_trade(
        self, signal: str, ticker: str, qty: int, predict_fk: str, orchestrator_fk: str
    ) -> Dict[str, Any]:
        """
        Executes a trade based on the given signal, ticker, and quantity.

        Args:
            signal (str): The trade signal, either 'buy' or 'sell'.
            ticker (str): The ticker symbol of the stock to trade.
            qty (int): The quantity of stock to trade.
            predict_fk (str): Foreign key reference to prediction.
            orchestrator_fk (str): Foreign key reference to orchestrator.

        Returns:
            Dict[str, Any]: The status of the trade execution.
        """
        try:
            logging.info(f"signal submitted: {signal} on {ticker}")
            qty = int(qty)
            self.ticker = ticker

            # Sync with Alpaca
            self._update_order_status()
            self._update_market_status()
            self._update_position_status()

            # Check if the market is open
            if not self.market_status["market_open_bool"]:
                logging.warning(f"The market is not currently open.")
                return {"status": "Market is closed. Trade cannot be executed"}

            order = self._submit_order(signal, qty)
            order_status = self._wait_for_order_fill(order.id)

            meta = json.dumps(order_status.to_dict())

            # Prepare the data for insertion
            execution_time = datetime.now(timezone.utc)
            pk = str(int(time.time())) + self.ticker

            trade_data = {
                "pk": pk,
                "date": execution_time.date(),
                "execution_time": execution_time,
                "predict_fk": predict_fk,
                "orchestrator_fk": orchestrator_fk,
                "ticker": ticker,
                "qty": qty,
                "signal": signal,
                "meta": meta,
            }

            # Insert data into the transaction_events table
            trade_data_df = pd.DataFrame([trade_data])
            trade_data_df.to_sql(
                name="transaction_events",
                con=self.engine,
                schema="transactions",
                if_exists="append",
                index=False,
            )
            return {"status": order_status.status, "pk": pk}
        except Exception as e:
            logging.error(f"Failed to execute trade: {e}")
            return {"status": "Failed", "error": str(e)}
        finally:
            self.ticker = None
            self.market_status = {}
            self.order_status = {}
            self.position_status = {}

    def _submit_order(self, signal: str, qty: int) -> Order:
        """
        Submits a trade order based on the given signal, ticker, and quantity.
        If sell
            if zero owned, do nothing
            if qty -1, sell all
            if qty_owned less than qty, error
            else sell qty
        if buy
            if adequate buying power, buy
            else error

        Args:
            signal (str): The trade signal, either 'buy' or 'sell'.
            qty (int): The quantity of stock to trade.

        Returns:
            Order (obj): The submitted order.
        """
        if signal not in ["buy", "sell"]:
            logging.error(f"Invalid signal: {signal}")
            raise ValueError(f"Invalid signal: {signal}")

        try:
            qty_locked = self.position_status["locked_shares"]
            qty_owned = self.position_status["owned_shares"]
            if qty_locked > 0:
                self.cancel_open_orders(self.ticker, retain_attributes=True)
                logging.warning(
                    f"{self.ticker} had locked shares. Cancel command sent."
                )
                self._update_position_status()
                qty_locked = self.position_status["locked_shares"]
                if qty_locked > 0:
                    logging.warning(f"Unsuccessful. {qty_locked} locked shares remain.")
                else:
                    logging.info(f"{self.ticker} locked shares successfully unlocked.")

            if signal == "sell":
                if qty_owned == 0:
                    logging.info("No shares to sell")
                    return Order(id=None, status="Cancel", symbol=self.ticker)

                elif qty == -1:
                    order = self._submit_sell_order(qty_owned)
                    logging.info(
                        f"Sell order submitted for all owned shares: {order.id}"
                    )
                    return order
                # Check if enough is owned to sell
                elif qty_owned < qty:
                    raise ValueError(
                        f"Quantity specified to sell is more than quantity owned. Qty to sell: {qty}; Qty owned: {qty_owned}"
                    )
                else:
                    order = self._submit_sell_order(qty)
                    logging.info(f"Sell order submitted for {qty} shares: {order.id}")
                    return order

            # Check if enough buying power to purchase
            buying_power = self.position_status["buying_power"]
            current_price = self.market_status["current_price"]
            if buying_power < qty * current_price:
                raise ValueError(
                    f"Buying power is less than qty * current_price. Buying power: {buying_power}; Cost: {qty * current_price}"
                )

            # Prepare and submit order
            take_profit_price, stop_loss_price = self._calculate_bracket_prices(
                signal, current_price
            )
            order = self._submit_bracket_order(qty, take_profit_price, stop_loss_price)
            logging.info(f"Buy order submitted: {order.id}")
            return order
        except Exception as e:
            logging.error(f"Failed to submit order: {e}")
            raise

    def _update_order_status(self) -> None:
        """
        Pulls all open orders for a given ticker and updates the order status.
        """
        try:
            open_orders = self.api.list_orders(status="open", symbols=[self.ticker])
            logging.info(f"Open orders for {self.ticker}: {open_orders}")

            self.order_status = {
                "open_orders": open_orders,
                "open_orders_bool": len(open_orders) > 0,
            }
        except tradeapi.rest.APIError as e:
            logging.error(f"API error while checking open orders: {e}")
        except Exception as e:
            logging.error(f"Unexpected error while checking open orders: {e}")

    def _update_market_status(self) -> None:
        """
        Gets the current price of the given ticker and market status.
        """
        try:
            trade = self.api.get_latest_trade(self.ticker)
            current_price = trade.price if trade else None

            clock = self.api.get_clock()
            market_open_bool = clock.is_open if clock else None

            if current_price is None or market_open_bool is None:
                raise ValueError("Failed to retrieve valid market status")

            logging.info(f"Market state updated: market_open={market_open_bool}")

            self.market_status = {
                "current_price": current_price,
                "market_open_bool": market_open_bool,
            }
        except tradeapi.rest.APIError as e:
            logging.error(
                f"API error while fetching market status for {self.ticker}: {e}"
            )
        except Exception as e:
            logging.error(
                f"Unexpected error while fetching market status for {self.ticker}: {e}"
            )

    def _update_position_status(self) -> None:
        """
        Updates the position status for the given ticker, including owned shares, locked shares, available shares, and buying power.
        """
        try:
            account = self.api.get_account()
            positions = self.api.list_positions()

            owned_shares = 0
            available_shares = 0
            locked_shares = 0
            for position in positions:
                if position.symbol == self.ticker:
                    owned_shares = int(position.qty)
                    available_shares = int(position.qty_available)
                    locked_shares = owned_shares - available_shares

            buying_power = float(account.buying_power)

            self.position_status = {
                "owned_shares": owned_shares,
                "available_shares": available_shares,
                "locked_shares": locked_shares,
                "buying_power": buying_power,
            }

            logging.info(f"Position status updated: {self.position_status}")
        except tradeapi.rest.APIError as e:
            logging.error(
                f"API error while fetching position status for {self.ticker}: {e}"
            )
        except Exception as e:
            logging.error(
                f"Unexpected error while fetching position status for {self.ticker}: {e}"
            )

    def _wait_for_order_fill(self, order_id: str) -> Order:
        """
        Waits for the order to be filled and returns the final order details.

        Args:
            order_id (str): The ID of the submitted order.

        Returns:
            Order (obj): The final details of the filled order.
        """
        start_time = time.time()
        try:
            if order_id is None:
                return Order(id=None, status="Cancel", symbol=self.ticker)
            else:
                order_details = self.api.get_order(order_id)
                while order_details.status in ["new", "accepted", "partially_filled"]:
                    logging.info("Checking order status...")
                    time.sleep(2)
                    order_details = self.api.get_order(order_id)

                    if time.time() - start_time > self.open_order_wait:
                        logging.warning(
                            f"Order {order_id} is taking too long. Cancelling order..."
                        )
                        self.api.cancel_order(order_id)
                        order_details = self.api.get_order(order_id)
                        break

                logging.info(f"Order {order_id} final details: {order_details}")
                return Order(
                    id=order_details.id,
                    status=order_details.status,
                    symbol=order_details.symbol,
                    raw_data=order_details._raw,
                )
        except Exception as e:
            logging.error(f"Failed to wait for order fill: {e}")
            raise

    def _calculate_bracket_prices(
        self, signal: str, current_price: float
    ) -> Tuple[float, float]:
        """
        Calculates take profit and stop loss prices based on the current price and trade signal.

        Args:
            signal (str): The trade signal, either 'buy' or 'sell'.
            current_price (float): The current market price of the stock.

        Returns:
            Tuple[float, float]: The take profit and stop loss prices.
        """
        if signal == "buy":
            take_profit_price = self._calculate_take_profit_price(
                self.take_profit_multiplier_buy, current_price
            )
            stop_loss_price = self._calculate_stop_loss_price(
                self.stop_loss_multiplier_buy, current_price
            )
        else:
            take_profit_price = self._calculate_take_profit_price(
                self.take_profit_multiplier_sell, current_price
            )
            stop_loss_price = self._calculate_stop_loss_price(
                self.stop_loss_multiplier_sell, current_price
            )

        self._validate_bracket_prices(
            signal, current_price, take_profit_price, stop_loss_price
        )
        return take_profit_price, stop_loss_price

    def _calculate_take_profit_price(
        self, multiplier: float, current_price: float
    ) -> float:
        """
        Calculates the take profit price based on the current price and multiplier.

        Args:
            multiplier (float): The multiplier to calculate the take profit price.
            current_price (float): The current market price of the stock.

        Returns:
            float: The calculated take profit price.
        """
        return round(float(current_price) * float(multiplier), 2)

    def _calculate_stop_loss_price(
        self, multiplier: float, current_price: float
    ) -> float:
        """
        Calculates the stop loss price based on the current price and multiplier.

        Args:
            multiplier (float): The multiplier to calculate the stop loss price.
            current_price (float): The current market price of the stock.

        Returns:
            float: The calculated stop loss price.
        """
        return round(float(current_price) * float(multiplier), 2)

    def _validate_bracket_prices(
        self,
        signal: str,
        current_price: float,
        take_profit_price: float,
        stop_loss_price: float,
    ) -> None:
        """
        Validates the calculated bracket prices to ensure they are logical based on the current price and trade signal.

        Args:
            signal (str): The trade signal, either 'buy' or 'sell'.
            current_price (float): The current market price of the stock.
            take_profit_price (float): The calculated take profit price.
            stop_loss_price (float): The calculated stop loss price.

        Raises:
            ValueError: If the bracket prices are not valid.
        """
        if signal == "buy":
            if take_profit_price <= current_price or stop_loss_price >= current_price:
                raise ValueError(
                    f"Invalid bracket prices for buy: take_profit_price ({take_profit_price}) must be > current_price and stop_loss_price ({stop_loss_price}) must be < current_price"
                )
        else:
            if take_profit_price >= current_price or stop_loss_price <= current_price:
                raise ValueError(
                    f"Invalid bracket prices for sell: take_profit_price ({take_profit_price}) must be < current_price and stop_loss_price ({stop_loss_price}) must be > current_price"
                )

    def _submit_bracket_order(
        self, qty: int, take_profit_price: float, stop_loss_price: float
    ) -> Order:
        """
        Submits a bracket order with take profit and stop loss prices.

        Args:
            qty (int): The quantity of stock to trade.
            take_profit_price (float): The take profit price.
            stop_loss_price (float): The stop loss price.

        Returns:
            Order (obj): The submitted order.
        """
        try:
            order = self.api.submit_order(
                symbol=self.ticker,
                qty=qty,
                side="buy",
                type="market",
                time_in_force="gtc",
                order_class="bracket",
                take_profit=dict(limit_price=take_profit_price),
                stop_loss=dict(stop_price=stop_loss_price),
            )
            logging.info(f"Bracket order submitted: {order.id}")
            return Order(
                id=order.id,
                status=order.status,
                symbol=order.symbol,
                raw_data=order._raw,
            )
        except Exception as e:
            logging.error(f"Failed to submit bracket order: {e}")
            raise

    def _submit_sell_order(self, qty: int) -> Order:
        """
        Submits a sell order for the given ticker and quantity.

        Args:
            qty (int): The quantity of stock to sell.

        Returns:
            Order (obj): The submitted sell order.
        """
        try:
            order = self.api.submit_order(
                symbol=self.ticker,
                qty=qty,
                side="sell",
                type="market",
                time_in_force="gtc",
            )
            logging.info(f"Sell order submitted: {order.id}")
            return Order(
                id=order.id,
                status=order.status,
                symbol=order.symbol,
                raw_data=order._raw,
            )
        except Exception as e:
            logging.error(f"Failed to submit sell order: {e}")
            raise


if __name__ == "__main__":
    setup_config = {
        "alpaca": {
            "api_key": "your_api_key_id",
            "api_secret": "your_api_secret_id",
            "base_url": "https://paper-api.alpaca.markets",
        },
        "project_id": "your_gcp_project_id",
    }

    methods_config = {
        "ticker": "AAPL",
        "open_order_wait": 300,
        "take_profit_multiplier_sell": 1.02,
        "stop_loss_multiplier_sell": 0.98,
        "take_profit_multiplier_buy": 1.02,
        "stop_loss_multiplier_buy": 0.98,
    }

    trader = TradeExecutor(setup_config, methods_config)
    trader.execute_trade("buy", "AAPL", 10)
