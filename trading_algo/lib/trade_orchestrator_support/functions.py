from datetime import datetime, timezone
import time
import pandas as pd
from sqlalchemy import create_engine
from zoneinfo import ZoneInfo
from trading_algo.lib.trade_orchestrator_support.models import TickerOperator, TickerOperatorFactory


def log_orchestration_run(orchestration_type: str, db_url: str) -> str:
    orchestrator_pk = str(int(time.time())) + "ORCHE"
    execution_time = datetime.now(timezone.utc)

    orchestration_data = {
        "pk": orchestrator_pk,
        "date": execution_time.date(),
        "execution_time": execution_time,
        "type": orchestration_type,
    }
    # Convert orchestration data dictionary to DataFrame
    trade_data_df = pd.DataFrame([orchestration_data])

    # Store the data in the PostgreSQL database
    engine = create_engine(db_url)

    trade_data_df.to_sql(
        name="orchestrator_events",
        con=engine,
        schema="transactions",
        if_exists="append",
        index=False,
    )
    engine.dispose()

    return orchestrator_pk


def instantiate_ticker_operators(
    orche_conf: dict, orchestration_type: str
) -> list[TickerOperator]:
    """Quick funct to adhere to DRY and changing in only one place.
    Instantiates ticker operators into list and returns, along with logging orchestration.

    Args:
        orche_conf (dict): orchestration configs
        orchestration_type (str): orchestration type

    Returns:
        list[TickerOperator]: List of ticker operators, 1 for each specified in the orche_conf tickers
    """
    orchestrator_fk = log_orchestration_run(
        orchestration_type, orche_conf["DATABASE_URL"]
    )
    ticker_operator_factory = TickerOperatorFactory(orche_conf, orchestrator_fk)
    ticker_operators = ticker_operator_factory.create_ticker_operators()
    return ticker_operators


# Serial issues with lambda functions. So created a dedicated function
def get_new_york_time():
    return datetime.now(ZoneInfo("America/New_York"))
