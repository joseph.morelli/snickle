from datetime import datetime, timedelta
import requests
import logging
from sqlalchemy import create_engine
import json


class TickerOperator:
    """TickerOperators perform all actions for individual tickers
    instantiated by TickerOperatorFactory
    Interacts with flask backend
    """

    def __init__(self, orche_conf, ticker, orchestrator_fk):
        self.ticker = ticker
        self.buy_threshold = orche_conf["buy_threshold"]
        self.sell_threshold = orche_conf["sell_threshold"]
        self.base_url = orche_conf["BASE_URL"]
        self.train_days = orche_conf["train_days"]
        self.orchestrator_fk = orchestrator_fk

    def _trade_logic(self, prediction: float):
        """determine quantity and signal

        Args:
            prediction (float): model prediction for price increase of ticker

        Returns:
            str:  buy/sell/hold signal
        """
        quantity: int = 1

        if prediction > self.buy_threshold:
            signal = "buy"
        elif prediction < self.sell_threshold:
            signal = "sell"
        else:
            signal = "hold"

        return quantity, signal

    def _cancel_open(self):
        """Cancel open orders"""
        cancel_data = {
            "ticker": self.ticker,
            "order_types": None,
        }
        cancel_response = requests.post(
            f"{self.base_url}/trader/cancel-open", json=cancel_data
        )
        logging.info(
            f"Cancelled opened orders for {self.ticker}, response: {cancel_response.json()}"
        )

    def train_model(self):
        """Submit train request to flask backend"""
        end_date = datetime.today()
        start_date = end_date - timedelta(days=self.train_days)
        start_date_str = start_date.strftime("%Y-%m-%d")
        end_date_str = end_date.strftime("%Y-%m-%d")

        data = {
            "ticker": self.ticker,
            "start_date": start_date_str,
            "end_date": end_date_str,
            "orchestrator_fk": self.orchestrator_fk,
        }
        response = requests.post(f"{self.base_url}/model/train", json=data)
        logging.info(f"Training response for {self.ticker}: {response.json()}")

    def end_of_day_cleanup(self):
        """Cancel all open orders. Then sell all owned shares"""

        self._cancel_open()
        trade_data = {
            "signal": "sell",
            "ticker": self.ticker,
            "qty": -1,
            "predict_fk": None,
            "orchestrator_fk": self.orchestrator_fk,
        }
        trade_response = requests.post(
            f"{self.base_url}/trader/make-trade", json=trade_data
        )
        logging.info(
            f"End of day trade response for {self.ticker}: sell all, {trade_response.json()}"
        )

    def execute(self):
        """Get prediction from flask app
        and execute buy/sell/hold through flask app
        """
        predict_data = {"ticker": self.ticker, "orchestrator_fk": self.orchestrator_fk}
        predict_response = requests.post(
            f"{self.base_url}/model/predict", json=predict_data
        )
        prediction_load = predict_response.json()
        prediction = prediction_load.get("prediction", 0)

        quantity, signal = self._trade_logic(prediction)
        logging.info(
            f"Prediction and signal for {self.ticker}. pred:{prediction}, signal:{signal}"
        )

        trade_data = {
            "signal": signal,
            "ticker": self.ticker,
            "qty": quantity,
            "predict_fk": prediction_load.get("pk"),
            "orchestrator_fk": self.orchestrator_fk,
        }

        if signal == "buy":
            trade_response = requests.post(
                f"{self.base_url}/trader/make-trade", json=trade_data
            )
            logging.info(
                f"Trade response for {self.ticker}: {signal}, {trade_response.json()}"
            )
        elif signal == "sell":
            self._cancel_open()

            trade_data["qty"] = -1

            trade_response = requests.post(
                f"{self.base_url}/trader/make-trade", json=trade_data
            )
            logging.info(
                f"Trade response for {self.ticker}: {signal}, {trade_response.json()}"
            )
        else:
            logging.info(f"No action, holding strong")


class TickerOperatorFactory:
    """Factory class for instantiating ticker operators
    Gets ticker list from Flask app
    passes orchestration config to each TickerOperator
    Separates bulk creation from functionality for TickerOperator
    """

    def __init__(self, orche_conf, orchestrator_fk):
        self.orche_conf = orche_conf
        self.base_url = orche_conf["BASE_URL"]
        self.orchestrator_fk = orchestrator_fk
        self.tickers = self.get_active_tickers()

    def get_active_tickers(self):
        """Get active tickers from DB for orchestration run

        Returns:
            list: list of active tickers from DB
        """
        active_tickers_response = requests.get(
            f"{self.base_url}/orchestrator/active-tickers"
        )
        active_tickers_list = active_tickers_response.json()
        return active_tickers_list

    def create_ticker_operators(self) -> list[TickerOperator]:
        """Instantiate ticker operators and return list

        Returns:
            list[TickerOperator]: list of TickerOperators for all active Tickers
        """
        ticker_operator_list = []
        for ticker in self.tickers:
            ticker_operator = TickerOperator(
                self.orche_conf, ticker, self.orchestrator_fk
            )
            ticker_operator_list.append(ticker_operator)
        return ticker_operator_list
