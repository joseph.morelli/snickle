import yfinance as yf
import pandas as pd
from datetime import timedelta
import time
import logging

# Configure logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def get_data_in_chunks(
    ticker,
    start_date,
    end_date,
    run_type,
    chunk_size_days=7,
    sleep_time=5,
    max_retries=3,
):
    """
    Fetch data in chunks to avoid hitting the API limit.

    :param ticker: The stock ticker symbol.
    :param start_date: The start datetime for fetching data.
    :param end_date: The end datetime for fetching data.
    :param run_type: The type of run ('serve' or 'train').
    :param chunk_size_days: The number of days in each data chunk.
    :param sleep_time: The time to sleep between API calls to avoid rate limits.
    :param max_retries: The maximum number of retries for API calls.
    :return: A DataFrame containing the combined data.
    """
    chunk_size = timedelta(days=chunk_size_days)
    current_date = start_date
    data_chunks = []

    if run_type not in ["serve", "train"]:
        logging.error("Invalid run_type specified. Must be 'serve' or 'train'.")
        return pd.DataFrame()

    if start_date >= end_date:
        raise ValueError(
            f"start_date ({start_date}) must be before end_date ({end_date})"
        )

    if run_type == "serve":
        try:
            logging.info(f"Fetching data for {ticker} from {start_date} to {end_date}")
            data = fetch_minute_data(ticker, start_date, end_date, max_retries)
            data["unixtime"] = data.index.astype("int64") // 10**9
            return data
        except Exception as e:
            logging.error(
                f"Error fetching data for {ticker} from {start_date} to {end_date}: {e}"
            )
            return pd.DataFrame()

    elif run_type == "train":
        while current_date <= end_date:
            next_date = min(current_date + chunk_size, end_date)
            logging.info(f"Fetching data from {current_date} to {next_date}")

            try:
                data = fetch_minute_data(ticker, current_date, next_date, max_retries)
                if data is not None and not data.empty:
                    data_chunks.append(data)
                else:
                    logging.warning(
                        f"No data returned for {ticker} from {current_date} to {next_date}"
                    )
            except Exception as e:
                logging.error(
                    f"Error fetching data for chunk {current_date} to {next_date}: {e}"
                )
                break

            current_date = next_date + timedelta(days=1)
            time.sleep(sleep_time)

        if data_chunks:
            combined_data = pd.concat(data_chunks)
            combined_data["unixtime"] = combined_data.index.astype("int64") // 10**9
            combined_data = combined_data.reset_index(drop=True)
            logging.info("Successfully fetched and combined all data chunks")
            return combined_data
        else:
            logging.error("No data was fetched.")
            return pd.DataFrame()


def fetch_minute_data(ticker, start, end, max_retries=3):
    """
    Fetch minute-level data for a given ticker between start and end dates.

    :param ticker: The stock ticker symbol.
    :param start: The start date for fetching data. UTC
    :param end: The end date for fetching data. UTC
    :param max_retries: The maximum number of retries for API calls.
    :return: A DataFrame containing the fetched data.
    """
    retries = 0
    while retries < max_retries:
        try:
            data = yf.download(ticker, start=start, end=end, interval="1m")
            logging.info(f"Fetched data for {ticker} from {start} to {end}")
            return data
        except Exception as e:
            retries += 1
            logging.error(
                f"Failed to fetch data for {ticker} from {start} to {end} (attempt {retries} of {max_retries}): {e}"
            )
            time.sleep(2 * retries)  # Exponential backoff
    raise Exception(
        f"Exceeded maximum retries for fetching data for {ticker} from {start} to {end}"
    )


if __name__ == "__main__":
    # Example usage
    ticker = "AAPL"
    start_date = pd.Timestamp("2024-08-10")
    end_date = pd.Timestamp("2024-08-30")

    data = get_data_in_chunks(ticker, start_date, end_date, run_type="train")
    if not data.empty:
        logging.info("Fetched data:")
        logging.info(data.head())
    else:
        logging.error("No data fetched.")
