"""
Create general model strategies for use within Snickle orchestration and run_model_simulation.py

- Model strategies are specified inside the general configs.yaml
- Any new model strategy must be added to the MODEL_STRATEGIES factory
"""

import logging
import os
import time
import json
import joblib
import numpy as np
import pandas as pd
import pandas_ta as ta

from abc import ABC, abstractmethod
from datetime import datetime, timezone, timedelta
from typing import Union, List
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.class_weight import compute_class_weight

from trading_algo.configs.load_config import load_model_config, load_config
from trading_algo.lib.dataclasses.prediction import Prediction
from trading_algo.lib.label_strategy import LABEL_STRATEGIES


class ModelStrategy(ABC):
    def __init__(self, ticker: str):
        self.ticker = ticker

    @abstractmethod
    def _generate_model_path(self):
        pass

    @abstractmethod
    def load_model_data(self):
        pass

    @abstractmethod
    def _set_model_config(self):
        pass

    @abstractmethod
    def _save_model_data_and_set_state(self, model_data):
        pass

    @abstractmethod
    def run_train_pipeline(self, data, orchestrator_fk):
        pass

    @abstractmethod
    def run_predict_pipeline(self, data, orchestrator_fk):
        pass

    @abstractmethod
    def run_simulation_pipeline(self, data, orchestrator_fk):
        pass

    @abstractmethod
    def _preprocess_data(self, data, run_mode: str):
        pass

    @abstractmethod
    def _train(self, preprocessed_data, orchestrator_fk):
        pass

    @abstractmethod
    def get_start_and_end_predict_times(self):
        pass

    @abstractmethod
    def _predict(
        self, data: pd.DataFrame, orchestrator_fk: str, return_array: bool
    ) -> Union[Prediction, List[Prediction]]:
        pass


class RandomForestModelStrategy(ModelStrategy):
    def __init__(self, ticker: str):
        super().__init__(ticker)
        self.model = None
        self.model_config = {}
        self.training_info = {}

    def _generate_model_path(self) -> str:
        trading_algo_dir = os.path.join(
            os.path.dirname(os.path.realpath(__file__)), ".."
        )
        model_path = os.path.join(trading_algo_dir, "models", f"rf_{self.ticker}.pkl")
        return model_path

    def load_model_data(self):
        """
        Loads the saved model from disk and sets the internal state.
        Raises:
            FileNotFoundError: If the model file does not exist.
        """
        try:
            model_data = joblib.load(self._generate_model_path())
            self._set_internal_state(model_data)
        except FileNotFoundError:
            err_msg = f"Model data not found at {self._generate_model_path()}"
            logging.error(err_msg)
            raise FileNotFoundError(err_msg)

    def _set_internal_state(self, model_data: dict) -> None:
        """
        Updates the internal state of the strategy with loaded model data.
        """
        self.training_info = model_data["training_info"]
        self.model = model_data["model"]
        self.model_config = model_data["model_config"]

    def _set_model_config(self) -> None:
        """
        Loads configuration parameters for this random forest model.
        """
        self.model_config = load_model_config("rf_config.yaml")

    def _save_model_data_and_set_state(self, model_data: dict) -> None:
        """
        Saves model data to disk and updates internal state.
        """
        joblib.dump(model_data, self._generate_model_path(), compress=3)
        self._set_internal_state(model_data)

    def run_train_pipeline(
            self,
            data: pd.DataFrame,
            orchestrator_fk: str,
    ) -> dict:
        """
        Feed in raw data for processing and training.

        Runs the training workflow:
          1. Sets the model configuration.
          2. Preprocesses the data for training.
          3. Trains the model and saves the results.
        Returns:
            A dictionary containing metadata about the training process.
        """
        self._set_model_config()
        preprocessed_data = self._preprocess_data(data, run_mode="train")
        logging.info(f"Data preprocessed for training. Shape: {preprocessed_data.shape}")

        train_event_dict = self._train(preprocessed_data, orchestrator_fk)
        return train_event_dict

    def run_predict_pipeline(self, data: pd.DataFrame, orchestrator_fk: str) -> Prediction:
        """
        Runs the prediction workflow:
          1. Validates model availability.
          2. Preprocesses data in 'predict' mode.
          3. Generates a single prediction.
        Returns:
            A single `Prediction` object.
        """
        if self.model is None:
            raise ValueError(
                "Model is not available. Please train the model first or load an existing model."
            )
        preprocessed_data = self._preprocess_data(data, run_mode="predict")
        logging.info(f"Data preprocessed for prediction. Shape: {preprocessed_data.shape}")

        prediction = self._predict(
            preprocessed_data, orchestrator_fk, return_array=False
        )
        return prediction

    def run_simulation_pipeline(self, data: pd.DataFrame, orchestrator_fk: str) -> List[Prediction]:
        """
        Runs the simulation workflow:
          1. Validates model availability.
          2. Preprocesses data in 'simulation' mode (avoiding dropna).
          3. Generates predictions for each row.
        Returns:
            A list of `Prediction` objects.
        """
        if self.model is None:
            raise ValueError(
                "Model is not available. Please train the model first or load an existing model."
            )
        preprocessed_data = self._preprocess_data(data, run_mode="simulation")
        logging.info(
            f"Data preprocessed for simulation. Shape: {preprocessed_data.shape}"
        )

        predictions = self._predict(
            preprocessed_data, orchestrator_fk, return_array=True
        )
        return predictions

    def _preprocess_data(self, data: pd.DataFrame, run_mode: str) -> pd.DataFrame:
        """
        Preprocesses the data by:
          - Ensuring data isn't empty.
          - Creating time-based features from 'unixtime'.
          - Applying requested indicators.
          - Handling NaN values according to run_mode.

        Raises:
            ValueError: If input data is empty or if data is empty after preprocessing.
        """
        if data.empty:
            logging.error("Input data is empty.")
            raise ValueError("Input data is empty.")

        # Create time-based features
        data = create_time_features(data)

        # Apply indicators
        data = apply_indicators(
            data,
            self.model_config["feature_params"],
            self.model_config["calculated_features"],
        )

        # Handle data differently based on run mode
        if run_mode == "predict":
            # Keep only the latest record for a single prediction
            max_index = data.index.max()
            data = data.loc[[max_index]]
            data = data.drop(columns=["unixtime"], errors="ignore")
            data = data.dropna()

        elif run_mode == "train":
            # Keep 'unixtime' for label weighting, but drop NaNs
            data = data.dropna()

        elif run_mode == "simulation":
            # Drop 'unixtime' but do NOT drop NaNs
            data = data.drop(columns=["unixtime"], errors="ignore")

        logging.info(f"Columns after preprocessing: {list(data.columns)}")

        if data.empty:
            logging.error("Data is empty after preprocessing. Please check the input data.")
            raise ValueError(
                "Data is empty after preprocessing. Please check the input data."
            )

        return data

    def _train(
            self,
            data: pd.DataFrame,
            orchestrator_fk: str,
    ) -> dict:
        """
        Trains the RandomForest model.
        Steps:
          1. Parse label strategy from config
          2. Create features and labels
          3. Computes weights and train
          4. Saves the model and returns metadata for logging / DB insertion.
        """
        try:
            # -------------------------------------------------
            # 1) Parse label strategy from config
            # -------------------------------------------------
            label_strategy_config = self.model_config["label_strategy"][0]
            label_strategy_name = label_strategy_config["name"]
            label_strategy_args = label_strategy_config.get("args", {})

            LabelStrat = LABEL_STRATEGIES.get(label_strategy_name)
            if not LabelStrat:
                raise ValueError(
                    f"Unknown label strategy: {label_strategy_name}. "
                    f"Please add it to LABEL_STRATEGIES dict."
                )

            # Instantiate the label strategy
            label_strategy = LabelStrat(**label_strategy_args)

            # -------------------------------------------------
            # 2) Create features and labels
            # -------------------------------------------------
            start_date = data["unixtime"].min()
            end_date = data["unixtime"].max()
            zscore_value = 1.96

            # *All* columns except 'unixtime'
            feature_set = data.drop(columns=["unixtime"]).columns.tolist()

            # Generate labels using the chosen strategy
            data_trimmed, y = label_strategy.generate_labels(data, filter_nulls=True)
            X = data_trimmed[feature_set]

            # -------------------------------------------------
            # 3) Compute sample weights, train RF
            # -------------------------------------------------
            normalized_time = (data["unixtime"] - start_date) / (end_date - start_date)
            recency_weights = 1 / (1 + (1 - normalized_time.iloc[:-label_strategy.cadence]))

            class_weights = compute_class_weight("balanced", classes=np.unique(y), y=y)
            class_weights_dict = dict(enumerate(class_weights))
            y_class_weights = y.map(class_weights_dict)
            final_weights = recency_weights * y_class_weights

            # Additional metadata
            volatility_threshold = calculate_volatility_threshold(data, zscore_value)

            # Model parameters
            model_params = {
                "n_estimators": 400,
                "max_depth": 10,
                "min_samples_split": 5,
                "min_samples_leaf": 2,
                "max_features": "sqrt",
                "bootstrap": True,
                "class_weight": None,
                "random_state": 42,
            }
            model = RandomForestClassifier(**model_params)
            model.fit(X, y, sample_weight=final_weights)

            # -------------------------------------------------
            # 4) Build training_info dict & save
            # -------------------------------------------------
            # Create unique pk for the training run
            pk = str(int(time.time())) + self.ticker

            # Convert to int for clarity
            start_date_int = int(start_date)
            end_date_int = int(end_date)
            execution_time = datetime.now(timezone.utc)

            train_info_dict = {
                "pk": pk,
                "orchestrator_fk": orchestrator_fk,
                "date": execution_time.date(),
                "execution_time": execution_time,
                "meta": {
                    "start_date": start_date_int,
                    "end_date": end_date_int,
                    "ticker": self.ticker,
                    "pred_record_req": self.model_config["pred_record_req"],
                    "calculated_features": self.model_config["calculated_features"],
                    "feature_params": self.model_config["feature_params"],
                    "features": feature_set,
                    "params": model_params,
                    "volatility_threshold": volatility_threshold,
                },
            }

            model_data = {
                "model": model,
                "training_info": train_info_dict,
                "model_config": self.model_config,
            }

            self._save_model_data_and_set_state(model_data)
            logging.info(f"Model and training data saved to {self._generate_model_path()}")

            # Prepare for DB insertion
            train_db_upload_dict = train_info_dict.copy()
            train_db_upload_dict["meta"] = json.dumps(train_db_upload_dict["meta"])
            return train_db_upload_dict

        except Exception as e:
            logging.error(f"Failed to train model: {e}")
            raise

    def get_start_and_end_predict_times(self):
        """
        Defines the earliest and latest times for data retrieval during prediction. Amounts to the minimum
            required data to create all features for a prediction

        Returns:
            Tuple[datetime, datetime]: (start_datetime, end_datetime)
        """
        end_datetime = datetime.now(timezone.utc) + timedelta(minutes=2)
        start_datetime = end_datetime - timedelta(
            minutes=(self.model_config["pred_record_req"] + 2)
        )
        return start_datetime, end_datetime

    def _predict(
            self, data: pd.DataFrame, orchestrator_fk: str, return_array: bool
    ) -> Union[Prediction, List[Prediction]]:
        """
        Internal method to make predictions using the trained model.

        Parameters
        ----------
        data : pd.DataFrame
            The preprocessed data to make the prediction on.
        orchestrator_fk: str
            Foreign orchestrator key.
        return_array: bool
            If True, return a list of Predictions (one for each row);
            if False, return a single Prediction (first row).

        Raises
        ------
        ValueError
            If the model is not available or the input data has an incorrect number of features.

        Returns
        -------
        Union[Prediction, List[Prediction]]
            A single `Prediction` or a list of `Prediction` objects.
        """
        if self.model is None:
            raise ValueError("Model is not available. Please train or load the model first.")

        # Check input features
        if data.shape[1] != self.model.n_features_in_:
            raise ValueError("Input data has an incorrect number of features.")

        X = data
        pred_proba = self.model.predict_proba(X)[:, 1]

        if return_array:
            prediction = [
                Prediction(
                    ticker=self.ticker,
                    orchestrator_fk=orchestrator_fk,
                    train_fk=self.training_info["pk"],
                    prediction=pred_proba[i],
                    strength=0.5,
                    meta=json.dumps({"pred": pred_proba[0]}),
                )
                for i in range(0, len(pred_proba))
            ]

        else:
            prediction = Prediction(
                ticker=self.ticker,
                orchestrator_fk=orchestrator_fk,
                train_fk=self.training_info["pk"],
                prediction=pred_proba[0],
                strength=0.5,
                meta=json.dumps({"pred": pred_proba[0]}),
            )

        return prediction


# Factory dictionary: known model strategies
MODEL_STRATEGIES = {
    "RandomForestModelStrategy": RandomForestModelStrategy,
}


def create_time_features(data: pd.DataFrame) -> pd.DataFrame:
    """
    Create day_of_week, hour_of_day, and minute_of_day features from 'unixtime'.

    Raises:
        ValueError: If 'unixtime' is not present in the dataframe.
    """
    if "unixtime" not in data.columns:
        err_msg = "'unixtime' column is missing from the data."
        logging.error(err_msg)
        raise ValueError(err_msg)

    data["datetime"] = pd.to_datetime(data["unixtime"], unit="s")
    data["day_of_week"] = data["datetime"].dt.dayofweek
    data["hour_of_day"] = data["datetime"].dt.hour
    data["minute_of_day"] = data["datetime"].dt.minute

    # Drop the temporary datetime column
    data = data.drop(columns=["datetime"])
    return data


def apply_indicators(
    data: pd.DataFrame, indicators_config: list, calculated_features: list
) -> pd.DataFrame:
    """
    Applies technical indicators to the data based on configuration.

    Parameters
    ----------
    data : pd.DataFrame
        The original dataframe with OHLCV data.
    indicators_config : list
        A list of dictionaries with 'name' and optional 'args' for each indicator.
    calculated_features : list
        A list of feature names that should be calculated.

    Returns
    -------
    pd.DataFrame
        Dataframe with new indicator columns added.
    """
    for indicator in indicators_config:
        func_name = indicator["name"]
        if func_name in calculated_features:
            args = indicator.get("args", {})
            func = globals().get(f"create_{func_name}")
            if func:
                try:
                    result = func(data, **args)
                    if isinstance(result, pd.DataFrame):
                        for col in result.columns:
                            data[f"{func_name.upper()}_{col}"] = result[col]
                    elif isinstance(result, tuple):
                        for i, col in enumerate(result):
                            data[f"{func_name.upper()}_{i}"] = col
                    else:
                        data[func_name.upper()] = result
                except Exception as e:
                    logging.error(f"Error applying {func_name} with args {args}: {e}")
            else:
                logging.error(f"Indicator function 'create_{func_name}' not found.")
        else:
            logging.warning(f"Indicator {func_name} not in calculated features list.")
    return data


# Indicator creation helper functions
def create_sma(df: pd.DataFrame, length: int = 50) -> pd.Series:
    try:
        return ta.sma(df["Close"], length=length)
    except Exception as e:
        logging.error(f"Error creating SMA: {e}")
        return pd.Series(dtype=float)


def create_ema(df: pd.DataFrame, length: int = 20) -> pd.Series:
    try:
        return ta.ema(df["Close"], length=length)
    except Exception as e:
        logging.error(f"Error creating EMA: {e}")
        return pd.Series(dtype=float)


def create_rsi(df: pd.DataFrame, length: int = 14) -> pd.Series:
    try:
        return ta.rsi(df["Close"], length=length)
    except Exception as e:
        logging.error(f"Error creating RSI: {e}")
        return pd.Series(dtype=float)


def create_bbands(df: pd.DataFrame, length: int = 20, std: int = 2) -> pd.DataFrame:
    try:
        return ta.bbands(df["Close"], length=length, std=std)
    except Exception as e:
        logging.error(f"Error creating BBands: {e}")
        return pd.DataFrame()


def create_atr(df: pd.DataFrame, length: int = 14) -> pd.Series:
    try:
        return ta.atr(df["High"], df["Low"], df["Close"], length=length)
    except Exception as e:
        logging.error(f"Error creating ATR: {e}")
        return pd.Series(dtype=float)


def create_obv(df: pd.DataFrame) -> pd.Series:
    try:
        return ta.obv(df["Close"], df["Volume"])
    except Exception as e:
        logging.error(f"Error creating OBV: {e}")
        return pd.Series(dtype=float)


def create_macd(
    df: pd.DataFrame, fast: int = 12, slow: int = 26, signal: int = 9
) -> pd.DataFrame:
    try:
        return ta.macd(df["Close"], fast=fast, slow=slow, signal=signal)
    except Exception as e:
        logging.error(f"Error creating MACD: {e}")
        return pd.DataFrame()


def create_stoch(
    df: pd.DataFrame, fastk: int = 14, slowk: int = 3, slowd: int = 3
) -> pd.DataFrame:
    try:
        return ta.stoch(
            df["High"], df["Low"], df["Close"], fastk=fastk, slowk=slowk, slowd=slowd
        )
    except Exception as e:
        logging.error(f"Error creating Stoch: {e}")
        return pd.DataFrame()


def calculate_volatility_threshold(data: pd.DataFrame, zscore_value: float) -> float:
    """
    Calculates a volume-based threshold using mean + (zscore_value * std).

    Parameters
    ----------
    data : pd.DataFrame
        Market data including a 'Volume' column.
    zscore_value : float
        Z-score cutoff (e.g., 1.96 for ~95% confidence).

    Returns
    -------
    float
        The calculated volume threshold.
    """
    volume_mean = data["Volume"].mean()
    volume_std = data["Volume"].std()
    threshold = volume_mean + zscore_value * volume_std
    logging.info(f"Calculated volatility threshold (Z-score={zscore_value}): {threshold}")
    return threshold
