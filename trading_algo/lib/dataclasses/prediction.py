from dataclasses import dataclass, field
from datetime import datetime, timezone
import time
import json
from typing import Any


@dataclass
class Prediction:
    ticker: str
    orchestrator_fk: str
    train_fk: str
    prediction: float
    strength: float
    meta: str
    execution_time: datetime = field(default_factory=lambda: datetime.now(timezone.utc))
    pk: str = field(init=False)
    date: datetime = field(init=False)

    def __post_init__(self):
        self.pk = f"{int(time.time())}{self.ticker}"
        self.date = self.execution_time.date()

    def __getitem__(self, key: str) -> Any:
        return getattr(self, key)

    def __setitem__(self, key: str, value: Any) -> None:
        setattr(self, key, value)

    def __contains__(self, key: str) -> bool:
        return hasattr(self, key)

    def get(self, key: str, default: Any = None) -> Any:
        return getattr(self, key, default)

    def items(self):
        return {
            field.name: getattr(self, field.name)
            for field in self.__dataclass_fields__.values()
        }.items()

    def to_dict(self):
        return {
            field.name: getattr(self, field.name)
            for field in self.__dataclass_fields__.values()
        }

    def to_response(self):
        return {
            field.name: getattr(self, field.name)
            for field in self.__dataclass_fields__.values()
            if field.name in ["prediction", "strength", "pk"]
        }
