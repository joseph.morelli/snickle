"""
Create general label strategies that can be used inside different model strategies
    and run_model_simulation.py

- Label strategies are specified inside the model-specific configurations
- Any new label strategy must be added to the LABEL_STRATEGIES factory
"""
import pandas as pd
from abc import ABC, abstractmethod


class LabelStrategy(ABC):
    """Abstract base class for label generation."""
    def __init__(self, cadence: int):
        self.cadence = cadence

    @abstractmethod
    def generate_labels(self, data: pd.DataFrame, filter_nulls) -> tuple[pd.DataFrame, pd.Series]:
        """Generate labels for training given a DataFrame."""
        pass


class BinaryFuturePriceChangeLabelStrategy(LabelStrategy):
    """Label is 1 if future price > current price, else 0."""
    def __init__(self, cadence: int):
        super().__init__(cadence)

    def generate_labels(self, data: pd.DataFrame, filter_nulls: bool = True) -> tuple[pd.DataFrame, pd.Series]:
        """

        Args:
            data: preprocessed_data and a 'Close' column for label generation
            filter_nulls: when True, filter out records from X and y that have no label

        Returns:
            data_trimmed, represents a pandas DataFrame with all columns
            y, represents labels 0, 1, and possibly NaNs depending on filter_nulls
        """

        if "Close" not in data.columns:
            raise ValueError("Data must contain 'Close' column for labeling.")

        if filter_nulls:
            y_full = (data["Close"].shift(-self.cadence) > data["Close"]).astype(int)
            y = y_full.iloc[:-self.cadence]
            data_trimmed = data.iloc[:-self.cadence]

            return data_trimmed, y
        else:
            y_full = (data["Close"].shift(-self.cadence) > data["Close"]).astype(int)

            return data, y_full


# Factory dictionary: known label strategies
LABEL_STRATEGIES = {
    "BinaryFuturePriceChangeLabelStrategy": BinaryFuturePriceChangeLabelStrategy,
}
