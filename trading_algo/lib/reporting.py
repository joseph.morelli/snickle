import pandas as pd
import json


def get_transaction_data(engine, start_date, end_date):
    query = f"""
        SELECT
            date::DATE AS trade_date,
            ticker,
            qty,
            signal,
            meta AS order_info
        FROM transactions.transaction_events
        WHERE 1=1
          AND date BETWEEN '{start_date}' AND '{end_date}'
          AND (meta::json)->>'id' IS NOT NULL
    """
    return pd.read_sql(query, engine)


def _safe_float(value):
    """Safely convert a value to float or return 0.0."""
    try:
        return float(value)
    except (ValueError, TypeError):
        return 0.0


def _safe_int(value):
    """Safely convert a value to int or return 0."""
    try:
        return int(value)
    except (ValueError, TypeError):
        return 0


def extract_order_info(order_info):
    """
    Returns a tuple of:
      (filled_avg_price: float, filled_qty: int, status: str, side: str)
    """
    # Parse string to dict if needed
    if isinstance(order_info, str):
        order_info = json.loads(order_info)

    # raw_data might also be a string; handle that
    raw_data = order_info.get("raw_data", {})
    if isinstance(raw_data, str):
        raw_data = json.loads(raw_data)

    # Fallback logic: use raw_data fields first, then top-level if missing
    filled_avg_price = _safe_float(raw_data.get("filled_avg_price", order_info.get("filled_avg_price", 0)))
    filled_qty = _safe_int(raw_data.get("filled_qty", order_info.get("filled_qty", 0)))

    # status & side fall back to top-level if raw_data is missing them
    status = raw_data.get("status", order_info.get("status"))
    side = raw_data.get("side", order_info.get("side"))

    return filled_avg_price, filled_qty, status, side


def calculate_metrics(transactions_df):
    """Calculate daily PnL metrics *by ticker* with buys as negative outflows and sells as positive inflows."""

    # 1) Extract the relevant fields from meta/JSON
    extracted = transactions_df["order_info"].apply(extract_order_info)

    transactions_df[["filled_avg_price", "filled_qty", "status", "side"]] = pd.DataFrame(extracted.tolist(), index=transactions_df.index)

    # 2) Filter out canceled orders (and anything else not 'filled' or 'partially_filled')
    valid_statuses = ["filled", "partially_filled"]
    transactions_df = transactions_df[transactions_df["status"].isin(valid_statuses)]

    # If nothing left, return empty frame with expected columns
    if transactions_df.empty:
        return pd.DataFrame(columns=[
            "trade_date",
            "ticker",
            "net_profit",
            "total_trades",
            "total_qty",
            "avg_profit_per_trade",
        ])

    # 3) Convert NaN to 0 in relevant columns
    transactions_df["qty"] = transactions_df["qty"].fillna(0)
    transactions_df["filled_avg_price"] = transactions_df["filled_avg_price"].fillna(0)
    transactions_df["filled_qty"] = transactions_df["filled_qty"].fillna(0)

    # 4) Calculate PnL per row using a sign based on buy/sell
    def calc_pnl(row):
        if row["side"] == "buy":
            return -row["filled_qty"] * row["filled_avg_price"]
        elif row["side"] == "sell":
            return row["filled_qty"] * row["filled_avg_price"]
        return 0

    transactions_df["pnl"] = transactions_df.apply(calc_pnl, axis=1)

    # 5) Group by (trade_date, ticker) to get daily totals
    grouped = transactions_df.groupby(["trade_date", "ticker"], dropna=False).agg(
        net_profit=("pnl", "sum"),
        total_trades=("signal", "count"),
        total_qty=("filled_qty", "sum")
    ).reset_index()

    # 6) Calculate average profit per trade
    grouped["avg_profit_per_trade"] = grouped.apply(
        lambda row: row["net_profit"] / row["total_trades"] if row["total_trades"] else 0,
        axis=1
    )

    # 7) Final columns in the desired order
    return grouped[
        [
            "trade_date",
            "ticker",
            "net_profit",
            "total_trades",
            "total_qty",
            "avg_profit_per_trade",
        ]
    ]


def format_metrics_for_d3(metrics_by_day_ticker):
    """
    Convert the daily-by-ticker metrics into a JSON-friendly format for D3 or similar libraries.
    """
    return {
        "net_profit": metrics_by_day_ticker[["trade_date", "ticker", "net_profit"]].to_dict(orient="records"),
        "total_trades": metrics_by_day_ticker[["trade_date", "ticker", "total_trades"]].to_dict(orient="records"),
        "total_qty": metrics_by_day_ticker[["trade_date", "ticker", "total_qty"]].to_dict(orient="records"),
        "avg_profit_per_trade": metrics_by_day_ticker[["trade_date", "ticker", "avg_profit_per_trade"]].to_dict(orient="records"),
    }
