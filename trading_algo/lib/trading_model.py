import logging
import pandas as pd
import numpy as np
from trading_algo.lib.data_pull import get_data_in_chunks
from trading_algo.lib.model_strategy import ModelStrategy
from trading_algo.lib.dataclasses.prediction import Prediction
from sqlalchemy import engine

# Configure logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


class TradingModel:
    def __init__(self, db_conn: engine, model_strategy: ModelStrategy):
        """
        Initializes the TradingModel with the provided configuration, ticker, and database connection.

        Parameters
        ----------
        setup_config : dict
            Configuration dictionary containing setup details.
        ticker : str
            The stock ticker symbol for the model.
        db_conn : engine
            The database connection engine.
        """
        self.model_strategy = model_strategy
        self.engine = db_conn

    def load_model(self) -> None:
        """
        Loads the model from the specified path.

        """
        self.model_strategy.load_model_data()
        logging.info(f"Model loaded from {self.model_strategy._generate_model_path()}")

    def train_model(self, start_date: str, end_date: str, orchestrator_fk: str) -> dict:
        """
        Orchestrates the trading model using the specified parameters and date range. Calls _train()

        Parameters
        ----------
        start_date : str
            The start date for the training data in YYYY-MM-DD format.
        end_date : str
            The end date for the training data in YYYY-MM-DD format.
        orchestrator_fk : str
            Foreign key from orchestrated runs

        Returns
        -------
        dict
            A dictionary indicating the training status.

        Raises
        ------
        Exception
            If there is an error during the training process.
        """
        # TODO: Add checks on length of raw and cleaned df to ensure both are within expectations
        try:

            raw_data = get_data_in_chunks(
                self.model_strategy.ticker, start_date, end_date, run_type="train"
            )
            logging.info(
                f"Latest raw data fetched for train. Data shape: {raw_data.shape}"
            )

            train_event_dict = self.model_strategy.run_train_pipeline(
                raw_data, orchestrator_fk
            )

            train_event_df = pd.DataFrame([train_event_dict])

            train_event_df.to_sql(
                name="train_events",
                con=self.engine,
                schema="models",
                if_exists="append",
                index=False
            )

            return {"train_status": "successful"}

        except Exception as e:
            logging.error(f"Failed to set up model retrain: {e}")
            raise

    def make_prediction(self, orchestrator_fk: str) -> Prediction:
        """
        Makes a prediction using the trained model for the specified ticker.

        Parameters
        ----------
        orchestrator_fk: str
            Foreign orchestrator key

        Returns
        -------
        Prediction
            An instance of the Prediction class containing the prediction results.

        Raises
        ------
        Exception
            If there is an error during the prediction process.
        """
        # TODO: Add checks on length of raw and cleaned df to ensure both are within expectations
        # TODO: Add logic around datetime
        try:

            # Testing only
            # end_datetime = datetime(
            #     year=2024, month=8, day=15, hour=15, minute=0
            # )
            if self.model_strategy.model is None:
                raise ValueError(
                    "Model data is not available. Please train the model first, or load an existing model."
                )

            start_datetime, end_datetime = (
                self.model_strategy.get_start_and_end_predict_times()
            )

            raw_data = get_data_in_chunks(
                self.model_strategy.ticker,
                start_datetime,
                end_datetime,
                run_type="serve",
            )
            logging.info(
                f"Latest raw data fetched for live prediction. Data shape: {raw_data.shape}"
            )
            prediction: Prediction = self.model_strategy.run_predict_pipeline(
                raw_data, orchestrator_fk
            )

            pred_df = pd.DataFrame([prediction.to_dict()])
            pred_df.to_sql(
                "predict_events", self.engine, "models", if_exists="append", index=False
            )
            logging.info("Prediction event saved to models.predict_events")

            return prediction

        except Exception as e:
            raise Exception(f"Error in predict function, {e}")


if __name__ == "__main__":
    # Example usage
    start_date = "2024-06-01"
    end_date = "2024-07-01"
    ticker = "RIVN"

    # Dummy data for illustration
    data = pd.DataFrame(
        {
            "Open": [100, 101, 102],
            "High": [105, 106, 107],
            "Low": [95, 96, 97],
            "Close": [102, 103, 104],
            "Adj Close": [102, 103, 104],
            "Volume": [1000, 1100, 1200],
            "SMA": [101, 102, 103],
            "RSI": [50, 51, 52],
        }
    )
    features = ["Open", "High", "Low", "Close", "Adj Close", "Volume", "SMA", "RSI"]

    model_path = "../models/random_forest_model.pkl"
    setup_config = {
        "save_model_data_file_path": "path/to/event_data.csv",
        "model_storage_path": model_path,
    }

    trading_model = TradingModel(setup_config, ticker)
    trading_model.train_model(data, features, {}, start_date, end_date, ticker)
    pred = trading_model.predict(data, end_date, ticker)
    logging.info(f"Prediction for {ticker} on {end_date}: {pred}")
