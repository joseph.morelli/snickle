import os 
import logging 
import yaml


def load_config():
    configs_directory = os.path.dirname(os.path.realpath(__file__))

    # Load configuration
    config_path = os.path.join(configs_directory, "configs.yaml")

    try:
        with open(config_path, "r") as config_file:
            config = yaml.safe_load(config_file)
            return config
    except Exception as e:
        logging.error(f"Failed to load config file: {e}")
        raise


def load_model_config(filename):
    configs_directory = os.path.dirname(os.path.realpath(__file__))

    # Load configuration
    config_path = os.path.join(configs_directory, "model", filename)

    try:
        with open(config_path, "r") as config_file:
            config = yaml.safe_load(config_file)
            return config
    except Exception as e:
        logging.error(f"Failed to load config file: {e}")
        raise

if __name__ == "__main__":
    load_config()
    