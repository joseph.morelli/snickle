setup:
  project_id: "example_project"
  DATABASE_URL: "postgresql://myuser:mypassword@postgres:5432/mydb"
  BASE_URL: "http://flask:8080"
  alpaca: # trading
    secret_name: "ALPACA_API_SECRETS"
    secret_value: '{"API_KEY":"YOUR_API_KEY","API_SECRET":"YOUR_API_SECRET","PAPER_ENDPOINT":"https://paper-api.alpaca.markets"}'

methods:
  open_order_wait: 10  # seconds to wait for an order to fill before cancelling it
  take_profit_multiplier_sell: 0.95  # sell at 5% below current value (i.e., 95% of the current price)
  stop_loss_multiplier_sell: 1.1  # sell at 10% above current value (i.e., 110% of the current price)
  take_profit_multiplier_buy: 1.1  # buy at 10% above current value (i.e., 110% of the current price)
  stop_loss_multiplier_buy: 0.95  # buy at 5% below current value (i.e., 95% of the current price)

orchestration:
  run_cadence: 2  # cadence (minutes) on which orchestrator is run
  sched_start_time_hour: 10  # ET, hour to start trading
  sched_start_time_minute: 30  # ET, minute to start trading
  sched_end_time_hour: 15  # ET, hour to end trading
  sched_end_time_minute: 59  # ET, minute to do the end-of-day sell
  train_days: 30  # Number of days to train models, anchors on today and looks back XX days
  train_on_startup: False  # Determine if the model should be trained on celery startup, generally not needed (will retrain anyway)
  sell_threshold: 0.46  # Sell when prediction below this value
  buy_threshold: 0.52  # Buy when prediction is above this value

simulation:
  train_window_size: 14
  ticker_list: ["RIVN", "SIRI"]
  run_cadence: 2  # cadence (minutes) on which simulated buy/sell is determined
  train_days: 30  # Number of days to train models, anchors on today and looks back XX days
  sell_threshold: 0.46  # Sell when prediction below this value
  buy_threshold: 0.52  # Buy when prediction is above this value

model_strategy:
  - name: RandomForestModelStrategy
    args:
      config: "rf_config.yaml"

polygon: # financial data (5 requests per hour)
  api_key: "POLYGON_API_KEY"

tiingo: # financal data (50 requests per hour)
  api_key: "TIINGO_API_KEY"

yfinance: #financial data (limit unknown)
  api_key: "YF_API_KEY"

alpha_vantage: # alpha vantage: finance data (25 requests per day)
  api_key: "ALPHA_VANTAGE_API_KEY"