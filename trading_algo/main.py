import time
import os
import logging
from flask import Flask, request, jsonify
from datetime import datetime, timedelta, timezone
from flask_cors import CORS
from trading_algo.lib.trading_model import TradingModel
from trading_algo.lib.trade_executor import TradeExecutor
from trading_algo.lib.reporting import get_transaction_data, calculate_metrics, format_metrics_for_d3
from trading_algo.lib.model_strategy import MODEL_STRATEGIES
from trading_algo.lib.label_strategy import LABEL_STRATEGIES
from trading_algo.lib.dataclasses.prediction import Prediction
from trading_algo.simulation.load_model_simulation_data import load_model_simulation_data
import argparse
from waitress import serve
from sqlalchemy import create_engine, text, inspect
import pandas as pd
from trading_algo.configs.load_config import load_config

# Configure logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

app = Flask(__name__)

CORS(app)

# Instantiate components
config = load_config()
setup_conf = config["setup"]
methods_conf = config["methods"]
model_strategy_conf = config["model_strategy"][0]
model_strategy_name = model_strategy_conf["name"]

ModelStrat = MODEL_STRATEGIES.get(model_strategy_name)
if not ModelStrat:
    raise ValueError(
        f"Unknown model strategy: {ModelStrat}. "
        f"Please add it to MODEL_STRATEGIES dict."
    )

model_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "models")
if not os.path.exists(model_dir):
    os.mkdir(model_dir)
models = {}

engine = create_engine(setup_conf["DATABASE_URL"])


@app.route("/db/schema-info", methods=["GET"])
def get_schema_info():
    """
    Return schema info (postgres)

    GET:
        No expected payload

    """
    try:
        inspector = inspect(engine)
        schema_info = {}

        for schema in inspector.get_schema_names():
            schema_info[schema] = {}
            for table_name in inspector.get_table_names(schema=schema):
                schema_info[schema][table_name] = {
                    "columns": [
                        col["name"]
                        for col in inspector.get_columns(table_name, schema=schema)
                    ]
                }

        return jsonify(schema_info)
    except Exception as e:
        logging.error(f"Error in get_schema_info: {e}")
        return jsonify({"error": str(e)}), 500


@app.route("/db/table-output", methods=["GET"])
def get_table_data():
    """
    Output contents of an entire table


    GET:
        Expects a JSON payload with the following keys:
            - schema (str): name of the schema
            - table_name (str): name of the table

    """
    try:
        data = request.json
        schema = data["schema"]
        table = data["table"]
        return_df: pd.DataFrame = pd.read_sql(f"select * from {schema}.{table}", engine)
        return jsonify(return_df.to_dict())
    except Exception as e:
        logging.error(f"Error in get_table_data: {e}")
        return jsonify({"error": str(e)}), 500


@app.route("/report/model-simulation-metrics", methods=["GET"])
def get_model_simulation_metrics():
    """
    Organize data from simulation_results.parquet, created from model simulated runs

    GET:
        No expected payload

    """
    try:
        simulation_results_data = load_model_simulation_data()
        simulation_results_data_dict_list = simulation_results_data.to_dict("records")
        # TODO implement custom json encoder to handle numpy arrays
        for record in simulation_results_data_dict_list:
            record["calculated_features"] = record["calculated_features"].tolist()
            record["feature_params"] = record["feature_params"].tolist()
            record["percentiles"] = record["percentiles"].tolist()

        return jsonify(simulation_results_data_dict_list)
    except Exception as e:
        logging.error(f"Error fetching model simulation metrics: {e}")
        return jsonify([])


@app.route("/report/transaction-metrics", methods=["GET"])
def get_transaction_metrics():
    """
    Output transaction metrics from orchestrated runs. Used for reporting

    GET:
        Expects a JSON payload with the following keys:
            - end_date (str): end date to pull reportings metrics
            - number_of_days (int): number of days to pull reportings metrics
    Returns:
        JSON response of day-level metrics
    """

    try:
        # Get end_date and number_of_days from the query parameters
        end_date_str = request.args.get(
            "end_date", datetime.now(timezone.utc).strftime("%Y-%m-%d")
        )
        number_of_days = int(request.args.get("number_of_days", 30))

        # Convert end_date to datetime object and calculate start_date
        end_date = datetime.strptime(end_date_str, "%Y-%m-%d")
        start_date = end_date - timedelta(days=number_of_days)

        # Extract transaction data
        transactions_df = get_transaction_data(engine, start_date, end_date)

        # Calculate metrics
        metrics_by_day = calculate_metrics(transactions_df)

        # Format metrics for D3 visualization
        metrics_data = format_metrics_for_d3(metrics_by_day)

        return jsonify(metrics_data)
    except Exception as e:
        logging.error(f"Error fetching transaction metrics: {e}")
        return jsonify({"error": str(e)}), 500


@app.route("/model/train", methods=["POST", "GET"])
def train_model():
    """
    Train a trading model for a given stock ticker within a specified date range.

    POST:
        Expects a JSON payload with the following keys:
            - ticker (str): The stock ticker symbol to train the model on.
            - start_date (str): The start date for the training data in YYYY-MM-DD format.
            - end_date (str): The end date for the training data in YYYY-MM-DD format.
            - orchestrator_fk (str): Foreign key from orchestrated runs
        The endpoint trains a model for the given ticker using the specified date range,
        calculated features, and other model parameters defined in the configs file. The
        trained model is cached for future predictions.

    GET:
        No expected payload
            - Retrieves and returns all training events stored in the database as a JSON response.

    Returns:
        JSON response indicating the training status for POST method.
        JSON response containing all training events for GET method.
    """
    try:
        if request.method == "POST":
            data = request.json
            logging.info(f"Received data for training: {data}")

            ticker = data["ticker"]
            orchestrator_fk: str = data["orchestrator_fk"]

            if "FRONTEND" in orchestrator_fk:
                end_date = datetime.today()
                start_date = end_date - timedelta(
                    days=config["orchestration"]["train_days"]
                )
                data["start_date"] = start_date.strftime("%Y-%m-%d")
                data["end_date"] = end_date.strftime("%Y-%m-%d")

            start_date = datetime.strptime(data["start_date"], "%Y-%m-%d")
            end_date = datetime.strptime(data["end_date"], "%Y-%m-%d")

            model_strategy = ModelStrat(ticker)

            trading_model = TradingModel(engine, model_strategy)
            train_status = trading_model.train_model(
                start_date,
                end_date,
                orchestrator_fk,
            )

            logging.info(f"Adding {ticker} to models cache")
            models[ticker] = trading_model
            logging.info("Training completed")

            return jsonify(train_status)

        elif request.method == "GET":
            return_df: pd.DataFrame = pd.read_sql(
                f"select * from models.train_events", engine
            )
            return jsonify(return_df.to_dict())

    except Exception as e:
        logging.error(f"Error in train: {e}")
        return jsonify({"error": str(e)}), 500


@app.route("/model/predict", methods=["POST", "GET"])
def predict():
    """
    Predict endpoint for making predictions using a trained model.

    POST:
        Expects a JSON payload with the following key:
            - ticker (str): The stock ticker symbol to make a prediction for.
            - orchestrator_fk (str): Foreign key from orchestrated runs
        The endpoint checks if the model for the given ticker is cached. If not,
        it loads the model from memory. Then, it makes a prediction using the
        model and returns the prediction in JSON format.

    GET:
        No payload expected
            - Retrieves and returns all prediction events stored in the database.

    Returns:
        JSON response with the prediction result for POST method.
        JSON response with all prediction events for GET method.
    """
    try:
        if request.method == "POST":
            data = request.json
            ticker = data["ticker"]
            orchestrator_fk = data["orchestrator_fk"]

            # Check model cache for model
            trading_model = models.get(ticker)

            logging.info(f"Received data for prediction: {data}")

            if not trading_model:
                try:
                    logging.info("Loading model from memory")
                    trading_model = TradingModel(
                        engine, ModelStrat(ticker)
                    )
                    trading_model.load_model()

                except Exception as e:
                    logging.error(f"Model not found for {ticker}")
                    return (
                        jsonify(
                            {
                                "error": f"Model for {ticker} not found, did you create it?, error: {e}"
                            }
                        ),
                        400,
                    )

                logging.info("Adding model to cache")
                models[ticker] = trading_model
            else:
                logging.info(f"Using cached model for {ticker}")

            prediction: Prediction = trading_model.make_prediction(orchestrator_fk)
            return jsonify(prediction.to_response())

        elif request.method == "GET":
            return_df: pd.DataFrame = pd.read_sql(
                f"select * from models.predict_events", engine
            )
            return jsonify(return_df.to_dict())

    except Exception as e:
        logging.error(f"Error in predict: {e}")
        return jsonify({"error": str(e)}), 500


@app.route("/trader/make-trade", methods=["POST", "GET"])
def trade():
    """
    Trade endpoint for executing trades based on prediction signals.

    POST:
        Expects a JSON payload with the following keys:
            - signal (str): The trade signal to execute (e.g., 'buy', 'sell').
            - ticker (str): The stock ticker symbol for the trade.
            - qty (int): The quantity of shares to trade.
            - predict_fk (int): The foreign key reference to the prediction event.
            - orchestrator_fk (str): Foreign key from orchestrated runs
        The endpoint executes the trade using the trade executor and returns the
        executed trade details in JSON format.

    GET:
        No payload expected
            - Retrieves and returns all transaction data stored in the database.

    Returns:
        JSON response with the executed trade details for POST method.
        JSON response with all transaction data for GET method.
    """
    try:
        if request.method == "POST":
            data = request.json
            logging.info(f"Received data for trading: {data}")

            signal = data["signal"]
            ticker = data["ticker"]
            qty = data["qty"]
            predict_fk = data["predict_fk"]
            orchestrator_fk = data["orchestrator_fk"]

            trade_executor = TradeExecutor(setup_conf, methods_conf, engine)
            executed_dict = trade_executor.execute_trade(
                signal, ticker, qty, predict_fk, orchestrator_fk
            )

            logging.info("Trade executed")

            return jsonify(executed_dict)

        elif request.method == "GET":
            return_df: pd.DataFrame = pd.read_sql(
                "select * from transactions.transaction_events", engine
            )
            return jsonify(return_df.to_dict())

    except Exception as e:
        logging.error(f"Error in trade: {e}")
        return jsonify({"error": str(e)}), 500


@app.route("/trader/cancel-open", methods=["POST"])
def cancel_open_orders():
    """
    Trade endpoint for cancelling open orders

    POST:
        Expects a JSON payload with the following keys:
            - ticker (str): The stock ticker symbol for the trade.
            - order_types (List[str], optional): The types of orders to cancel (e.g., ["limit", "stop"]). Defaults to None, which cancels all open orders.
        The endpoint cancels open orders specified. If not specified, cancel all open for a given ticker

    Returns:
        JSON response with the canceled details for POST method.
    """
    try:
        data = request.json
        logging.info(f"Received data for trading: {data}")

        ticker = data["ticker"]
        order_types = data["order_types"]

        trade_canceller = TradeExecutor(setup_conf, methods_conf, engine)
        cancelled_dict = trade_canceller.cancel_open_orders(ticker, order_types)

        logging.info("Open orders cancelled")

        return jsonify(cancelled_dict)

    except Exception as e:
        logging.error(f"Error in cancel orders: {e}")
        return jsonify({"error": str(e)}), 500


@app.route("/orchestrator/tickers", methods=["POST"])
def update_active_tickers():
    """
    Update tickers table with new inputs from front end
    Replaces table on each run

    POST:
        Format:
            [
                {
                    "active": false,
                    "ticker": "RIVN"
                },
                {
                    "active": true,
                    "ticker": "TSLA"
                }
            ]
    Returns:
        A copy of the input
    """
    data = request.json

    tickers_df = pd.DataFrame(data)

    tickers_df.to_sql(
        name="tickers",
        con=engine,
        schema="orchestrator",
        if_exists="replace",
        index=False,
    )
    logging.info("Received  upload request for scheduler")
    return jsonify(data), 200


@app.route("/orchestrator/tickers", methods=["GET"])
def retrieve_tickers():
    """
    Retrieve all tickers with their active status

    Returns:
        A list of dicts of format:
        [
            {
                "active": false,
                "ticker": "RIVN"
            },
            {
                "active": true,
                "ticker": "TSLA"
            }
        ]
    """
    tickers_df: pd.DataFrame = pd.read_sql(
        "Select ticker,active from orchestrator.tickers", con=engine
    )
    tickers_dict = tickers_df.to_dict("records")

    return jsonify(tickers_dict), 200


@app.route("/orchestrator/active-tickers", methods=["GET"])
def retrieve_active_tickers():
    """
    Retrieve all active tickers for TickerOperatorFactory to instantiate

    Returns:
        A list of active tickers
    """
    tickers_df: pd.DataFrame = pd.read_sql(
        "Select ticker,active from orchestrator.tickers where active = True", con=engine
    )
    tickers_list = tickers_df["ticker"].tolist()

    return jsonify(tickers_list), 200


if __name__ == "__main__":
    # Set up argument parser
    parser = argparse.ArgumentParser(
        description="Run the Flask app with optional debug mode"
    )
    parser.add_argument(
        "--debug", action="store_true", help="Run in debug mode with auto-reloading"
    )
    args = parser.parse_args()

    if args.debug:
        # Use Flask's built-in server with debug mode for auto-reloading
        print("Running in debug mode with auto-reloading")
        app.run(host="0.0.0.0", port=8080, debug=True)
    else:
        # Use Waitress for production
        print("Running in production mode with Waitress")
        serve(app, host="0.0.0.0", port=8080)
