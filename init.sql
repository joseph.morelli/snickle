CREATE SCHEMA IF NOT EXISTS transactions;
CREATE TABLE IF NOT EXISTS transactions.transaction_events (
    pk TEXT PRIMARY KEY,
    date DATE,
    execution_time TIMESTAMPTZ,
    predict_fk TEXT,
    orchestrator_fk TEXT,
    ticker TEXT,
    qty INTEGER,
    signal TEXT,
    meta JSONB
);
CREATE TABLE IF NOT EXISTS transactions.orchestrator_events (
    pk TEXT,  -- not unique due to multiple actions on startup
    date DATE,
    execution_time TIMESTAMPTZ,
    type TEXT
);

CREATE SCHEMA IF NOT EXISTS models;
CREATE TABLE IF NOT EXISTS models.train_events (
    pk TEXT PRIMARY KEY,
    orchestrator_fk TEXT,
    date DATE,
    execution_time TIMESTAMPTZ,
    meta JSONB
);
CREATE TABLE IF NOT EXISTS models.predict_events (
    pk TEXT PRIMARY KEY,
    orchestrator_fk TEXT,
    train_fk TEXT,
    ticker TEXT,
    prediction DOUBLE PRECISION,
    strength DOUBLE PRECISION,
    execution_time TIMESTAMPTZ,
    date DATE,
    meta JSONB
);

CREATE SCHEMA IF NOT EXISTS orchestrator;
CREATE TABLE IF NOT EXISTS orchestrator.tickers (
    active BOOLEAN,
    ticker TEXT
);

INSERT INTO orchestrator.tickers (active, ticker)
VALUES
    (TRUE, 'SIRI'),
    (TRUE, 'GME'),
    (FALSE, 'TSLA'),
    (TRUE, 'RIVN');

