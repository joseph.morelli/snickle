import React, { useState } from 'react';
import axios from 'axios';
import { TextField, Button, Grid } from '@mui/material';
import { DatePicker, DateField } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import dayjs from 'dayjs';

function Train({ onActionComplete }) {
  const [ticker, setTicker] = useState('');
  // const [startDate, setStartDate] = useState('');
  const [selectedStartDate, setSelectedStartDate] = useState(null);
  const [formattedStartDate, setFormattedStartDate] = useState('');

  const [selectedEndDate, setSelectedEndDate] = useState(null);
  const [formattedEndDate, setFormattedEndDate] = useState('');

  // const [endDate, setEndDate] = useState('');
  const [response, setResponse] = useState(null);


  const handleStartDateChange = (date) => {
    setSelectedStartDate(date);
    if (date) {
      // Format the date as MM/DD/YYYY
      const formatted = date.format('YYYY-MM-DD');
      setFormattedStartDate(formatted);

    } else {
      setFormattedStartDate('');
    }
  };


  const handleEndDateChange = (date) => {
    setSelectedEndDate(date);
    if (date) {
      // Format the date as MM/DD/YYYY
      const formatted = date.format('YYYY-MM-DD');
      setFormattedEndDate(formatted);

    } else {
      setFormattedEndDate('');
    }
  };


  const handleSubmit = async (e) => {
    e.preventDefault();
    setResponse(null); // Clear previous response
    try {
      const response = await axios.post('http://localhost:8080/model/train', {
        ticker,
        start_date: formattedStartDate,
        end_date: formattedEndDate,
      });
      setResponse(response.data);
      onActionComplete();
    } catch (error) {
      console.error(error);
      setResponse({ error: error.message });
    }
  };

  return (
    <div>
      <h2>Train Model</h2>
      <Grid container spacing={1}>
        {/* <input type="text" value={ticker} onChange={(e) => setTicker(e.target.value)} /> */}
        <Grid item xs={2.5}>
          <TextField id="outlined-basic" label="Ticker" variant="outlined"
            value={ticker} onChange={(e) => setTicker(e.target.value)}
          />
        </Grid>
        {/* <label>
          Start Date:
          <input type="date" value={startDate} onChange={(e) => setStartDate(e.target.value)} />
        </label> */}

        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <Grid item xs={2}>
            <DatePicker
              label="Start Date"
              value={selectedStartDate}
              onChange={handleStartDateChange}
              renderInput={(params) => <TextField {...params} />}
            />
          </Grid>
          <Grid item xs={0.5}></Grid>
          <Grid item xs={2}>
            <DatePicker
              label="End Date"
              value={selectedEndDate}
              onChange={handleEndDateChange}
              renderInput={(params) => <TextField {...params} />}
            />
          </Grid>
        </LocalizationProvider>
      </Grid>
      {/* <label>
          End Date:
          <input type="date" value={endDate} onChange={(e) => setEndDate(e.target.value)} />
        </label> */}
      {/* <button type="submit">Train</button> */}
      <br></br>
      <Button variant="contained" onClick={handleSubmit} color="inherit">Train</Button>

      {response && <pre>{JSON.stringify(response, null, 2)}</pre>}
    </div>
  );
}

export default Train;
