import React, { useState, useEffect } from 'react';
import TradingResultsDashboard from './TradingResultsDashboard';
import OrchestratorControl from './OrchestratorControl';

function HomePage() {

    const [simulation_data, setData] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/report/model-simulation-metrics')
            .then(response => response.json())
            .then(data => setData(data))
            .catch(error => console.error('Error fetching data:', error));
    }, []);

    return (
        <div>
            <h2>Orchestrator Control</h2>
            <OrchestratorControl />
            <h2>Trading Dashboard</h2>
            <TradingResultsDashboard />
        </div>
    );
}

export default HomePage;
