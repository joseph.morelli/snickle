import React, { useState } from 'react';
import axios from 'axios';
import { TextField, Button, Grid } from '@mui/material';

function Predict({ onActionComplete }) {
  const [ticker, setTicker] = useState('');
  const [date, setDate] = useState('');
  const [response, setResponse] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setResponse(null); // Clear previous response
    try {
      const response = await axios.post('http://localhost:8080/model/predict', {
        ticker,
        date,
      });
      setResponse(response.data);
      onActionComplete();
    } catch (error) {
      console.error(error);
      setResponse({ error: error.message });
    }
  };

  return (
    <div>
      <h2>Predict</h2>
      <Grid container spacing={1}>
        <Grid item sm={2.5}>
          <TextField
            id="outlined-ticker"
            label="Ticker"
            variant="outlined"
            value={ticker}
            onChange={(e) => setTicker(e.target.value)}
            fullWidth
          />
        </Grid>
        <Grid item sm={2.5}>
          <TextField
            id="outlined-date"
            label="Date"
            type="date"
            variant="outlined"
            value={date}
            onChange={(e) => setDate(e.target.value)}
            InputLabelProps={{
              shrink: true,
            }}
            fullWidth
          />
        </Grid>
      </Grid>
      <br />
      <Button variant="contained" onClick={handleSubmit} color="inherit">
        Predict
      </Button>
      {response && <pre>{JSON.stringify(response, null, 2)}</pre>}
    </div>
  );
}

export default Predict;
