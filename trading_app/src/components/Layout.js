import React, { useState, useMemo } from 'react';
import {
    AppBar, Toolbar, Typography, Button, Container, Box, CssBaseline,
    useMediaQuery, IconButton
} from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import { Link } from 'react-router-dom';

const Layout = ({ children }) => {
    const [mode, setMode] = useState('dark');
    const theme = useMemo(
        () => createTheme({ palette: { mode } }),
        [mode]
    );

    const toggleDarkMode = () => {
        setMode(prevMode => prevMode === 'light' ? 'dark' : 'light');
    };

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <Box sx={{ display: 'flex', flexDirection: 'column', minHeight: '100vh' }}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                            Snickle
                        </Typography>
                        <Button color="inherit" component={Link} to="/">Home</Button>
                        <Button color="inherit" component={Link} to="/simulations">Simulations</Button>
                        <Button color="inherit" component={Link} to="/testing">Testing</Button>
                        <IconButton sx={{ ml: 1 }} onClick={toggleDarkMode} color="inherit">
                            {theme.palette.mode === 'dark' ? <Brightness7Icon /> : <Brightness4Icon />}
                        </IconButton>
                    </Toolbar>
                </AppBar>

                <Container component="main" sx={{ mt: 8, mb: 2, flexGrow: 1 }}>
                    {children}
                </Container>

                <Box component="footer" sx={{ py: 3, px: 2, mt: 'auto', backgroundColor: 'background.paper' }}>
                    <Container maxWidth="sm">
                        <Typography variant="body2" color="text.secondary" align="center">
                            © 2024 MO-JO-JO-SE Snickle. All rights reserved.
                        </Typography>
                    </Container>
                </Box>
            </Box>
        </ThemeProvider>
    );
};

export default Layout;
