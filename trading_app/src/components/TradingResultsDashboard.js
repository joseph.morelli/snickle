import React, { useState, useEffect, useMemo } from 'react';
import {
    Box, Button, Card, CardContent, Typography, Select, MenuItem,
    Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper
} from '@mui/material';
import { LineChart, Line, XAxis, YAxis, Tooltip, CartesianGrid, ResponsiveContainer } from 'recharts';

const TradingResultsDashboard = () => {
    const [data, setData] = useState(null); // Initialize data as null
    const [selectedTicker, setSelectedTicker] = useState('All');
    const [selectedMetric, setSelectedMetric] = useState('avg_profit_per_trade');

    const fetchData = (endDate, numberOfDays) => {
        fetch(`http://localhost:8080/report/transaction-metrics?end_date=${endDate}&number_of_days=${numberOfDays}`)
            .then(response => response.json())
            .then(data => setData(data))
            .catch(error => console.error('Error fetching data:', error));
    };

    useEffect(() => {
        // Fetch default data for the last 60 days
        fetchData(new Date().toISOString().split('T')[0], 60);
    }, []);

    const tickers = useMemo(() => {
        if (!data || !data.net_profit) return ['All']; // Handle undefined data
        return ['All', ...new Set(data.net_profit.map(d => d.ticker))];
    }, [data]);

    const filteredData = useMemo(() => {
        if (!data || !data[selectedMetric]) return []; // Handle undefined data
        if (selectedTicker === 'All') {
            return data[selectedMetric]; // Return all data if "All" is selected
        }
        return data[selectedMetric]?.filter(d => d.ticker === selectedTicker) || [];
    }, [data, selectedTicker, selectedMetric]);

    // Group data by trade date
    const groupedData = useMemo(() => {
        if (!filteredData) return [];

        const grouped = {};
        filteredData.forEach(d => {
            const date = new Date(d.trade_date).toISOString().split('T')[0];
            if (!grouped[date]) grouped[date] = { trade_date: date };
            grouped[date][d.ticker] = d[selectedMetric]; // Add the metric for each ticker
        });

        return Object.values(grouped);
    }, [filteredData, selectedMetric]);

    const metrics = [
        { value: 'avg_profit_per_trade', label: 'Average Profit Per Trade' },
        { value: 'net_profit', label: 'Net Profit' },
        { value: 'total_qty', label: 'Total Quantity' },
        { value: 'total_trades', label: 'Total Trades' }
    ];

    // Check if data is still being loaded
    if (!data) {
        return <Typography>Loading data...</Typography>;
    }

    return (
        <Box sx={{ p: 4 }}>
            <Box sx={{ display: 'flex', gap: 2, mb: 4 }}>
                <Select
                    value={selectedTicker}
                    onChange={(e) => setSelectedTicker(e.target.value)}
                    sx={{ minWidth: 120 }}
                >
                    {tickers.map(ticker => (
                        <MenuItem key={ticker} value={ticker}>{ticker}</MenuItem>
                    ))}
                </Select>

                <Select
                    value={selectedMetric}
                    onChange={(e) => setSelectedMetric(e.target.value)}
                    sx={{ minWidth: 120 }}
                >
                    {metrics.map(metric => (
                        <MenuItem key={metric.value} value={metric.value}>{metric.label}</MenuItem>
                    ))}
                </Select>

                <Button
                    variant="contained"
                    color="inherit"
                    onClick={() => fetchData(new Date().toISOString().split('T')[0], 30)}
                >
                    Refresh
                </Button>
            </Box>

            <Box sx={{ mb: 4 }}>
                <Card>
                    <CardContent>
                        <Typography variant="h6" gutterBottom>Performance Over Time</Typography>
                        <ResponsiveContainer width="100%" height={300}>
                            <LineChart data={groupedData}>
                                <CartesianGrid strokeDasharray="3 3" />
                                <XAxis dataKey="trade_date" />
                                <YAxis />
                                <Tooltip />
                                {tickers.filter(t => t !== 'All').map((ticker) => (
                                    <Line
                                        key={ticker}
                                        type="monotone"
                                        dataKey={ticker}
                                        stroke={`#${Math.floor(Math.random() * 16777215).toString(16)}`}
                                        strokeWidth={3} // Thicker line
                                        name={ticker}
                                    />
                                ))}
                            </LineChart>
                        </ResponsiveContainer>
                    </CardContent>
                </Card>
            </Box>

            <Card>
                <CardContent>
                    <Typography variant="h6" gutterBottom>Detailed Results</Typography>
                    <TableContainer component={Paper}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Ticker</TableCell>
                                    <TableCell>Trade Date</TableCell>
                                    <TableCell>{metrics.find(m => m.value === selectedMetric)?.label}</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {filteredData.map((row, index) => (
                                    <TableRow key={index}>
                                        <TableCell>{row.ticker}</TableCell>
                                        <TableCell>{new Date(row.trade_date).toLocaleDateString()}</TableCell>
                                        <TableCell>{row[selectedMetric]}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </CardContent>
            </Card>
        </Box>
    );
};

export default TradingResultsDashboard;
