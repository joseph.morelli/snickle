import React, { useState, useEffect } from 'react';
import { Checkbox, FormControlLabel, Grid, List, ListItem, Typography, Box, TextField, Button, IconButton } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import FitnessCenterIcon from '@mui/icons-material/FitnessCenter';

const OrchestratorControl = () => {
    const [tickers, setTickers] = useState([]);
    const [ticker, setTicker] = useState('');

    useEffect(() => {
        fetchTickers();
    }, []);

    const fetchTickers = async () => {
        try {
            const response = await fetch('http://localhost:8080/orchestrator/tickers');
            if (!response.ok) throw new Error('Network response was not ok');
            const data = await response.json();
            setTickers(data);
        } catch (error) {
            console.error("Failed to fetch tickers:", error);
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const newTicker = {
            ticker: ticker,
            active: false
        };

        const updatedTickers = [...tickers, newTicker];
        setTickers(updatedTickers);
        setTicker('');

        await updateServer(updatedTickers);
    };

    const handleDeactivate = async (e) => {
        e.preventDefault();

        const updatedTickers = tickers.map(ticker => ({ ...ticker, active: false }));
        setTickers(updatedTickers);

        await updateServer(updatedTickers);
    };

    const handleCheckboxChange = async (index) => {
        const updatedTickers = [...tickers];
        updatedTickers[index].active = !updatedTickers[index].active;
        setTickers(updatedTickers);

        await updateServer(updatedTickers);
    };

    const handleDeleteTicker = async (index) => {
        const updatedTickers = tickers.filter((_, i) => i !== index);
        setTickers(updatedTickers);

        await updateServer(updatedTickers);
    };

    const handleTrainTicker = async (index) => {
        const tickerToTrain = { ...tickers[index] };
        tickerToTrain.orchestrator_fk = Math.floor(Date.now() / 1000).toString() + "FRONTEND";

        await submitServerTrain(tickerToTrain);
    };

    const updateServer = async (updatedTickers) => {
        try {
            const response = await fetch('http://localhost:8080/orchestrator/tickers', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(updatedTickers),
            });
            if (!response.ok) throw new Error('Failed to update tickers');
        } catch (error) {
            console.error("Failed to update tickers:", error);
        }
    };

    const submitServerTrain = async (ticker) => {
        try {
            const response = await fetch('http://localhost:8080/model/train', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(ticker),
            });
            if (!response.ok) throw new Error('Failed to train ticker');
        } catch (error) {
            console.error("Failed to train ticker:", error);
        }
    };

    return (
        <Box sx={{ p: 2 }}>
            <Typography variant="h4" gutterBottom>
                Ticker Controls
            </Typography>

            {/* Grid Headers */}
            <Grid container spacing={1} sx={{ mt: 2, mb: 1 }}>
                <Grid item xs={2}>
                    <Typography variant="subtitle1" fontWeight="bold">
                        Activation
                    </Typography>
                </Grid>
                <Grid item xs={4}>
                    <Typography variant="subtitle1" fontWeight="bold">
                        Ticker
                    </Typography>
                </Grid>
                <Grid item xs={2}>
                    <Typography variant="subtitle1" fontWeight="bold">
                        Train
                    </Typography>
                </Grid>
                <Grid item xs={2}>
                    <Typography variant="subtitle1" fontWeight="bold">
                        Remove
                    </Typography>
                </Grid>
            </Grid>

            <List>
                {tickers.map((ticker, index) => (
                    <ListItem key={ticker.ticker} disablePadding>
                        <Grid container spacing={1} alignItems="center">
                            <Grid item xs={2}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={ticker.active}
                                            onChange={() => handleCheckboxChange(index)}
                                            name={ticker.ticker}
                                        />
                                    }
                                    label=""
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>{ticker.ticker}</Typography>
                            </Grid>
                            <Grid item xs={2}>
                                <IconButton edge="end" aria-label="train" onClick={() => handleTrainTicker(index)}>
                                    <FitnessCenterIcon />
                                </IconButton>
                            </Grid>
                            <Grid item xs={2}>
                                <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteTicker(index)}>
                                    <DeleteIcon />
                                </IconButton>
                            </Grid>
                        </Grid>
                    </ListItem>
                ))}
            </List>

            <Grid container spacing={1} sx={{ mt: 4 }}>
                <Grid item xs={4}>
                    <TextField
                        id="outlined-basic"
                        label="Ticker"
                        variant="outlined"
                        value={ticker}
                        onChange={(e) => setTicker(e.target.value)}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={4}>
                    <Button variant="contained" onClick={handleSubmit} color="inherit" fullWidth>
                        Add Ticker
                    </Button>
                </Grid>
                <Grid item xs={4}>
                    <Button variant="contained" onClick={handleDeactivate} color="inherit" fullWidth>
                        Deactivate All Tickers
                    </Button>
                </Grid>
            </Grid>
        </Box>
    );
};

export default OrchestratorControl;
