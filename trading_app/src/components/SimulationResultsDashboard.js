import React, { useState, useEffect, useMemo } from 'react';
import {
    Box, Card, CardContent, Typography, Select, MenuItem,
    Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, FormControl, InputLabel
} from '@mui/material';
import { BarChart, Bar, XAxis, YAxis, Tooltip, CartesianGrid, ResponsiveContainer, LineChart, Line } from 'recharts';

const SimulationResultsDashboard = () => {
    const [data, setData] = useState([]);

    // Fetch the data on component mount
    useEffect(() => {
        fetch('http://localhost:8080/report/model-simulation-metrics')
            .then(response => response.json())
            .then(data => setData(data))
            .catch(error => console.error('Error fetching data:', error));
    }, []);

    const [selectedTicker, setSelectedTicker] = useState('All');
    const [selectedMetric, setSelectedMetric] = useState('total_pnl');
    const [selectedModelType, setSelectedModelType] = useState('All');
    const [selectedPk, setSelectedPk] = useState('All');

    // Generate options for model_type, pk, and ticker filters
    const modelTypes = useMemo(() => ['All', ...new Set(data.map(d => d.model_type || ''))], [data]);
    const tickers = useMemo(() => ['All', ...new Set(data.map(d => d.ticker || ''))], [data]);
    const pks = useMemo(() => ['All', ...new Set(data.map(d => d.pk || ''))], [data]);

    const filteredData = useMemo(() => {
        return data.filter(d =>
            (selectedTicker === 'All' || d.ticker === selectedTicker) &&
            (selectedModelType === 'All' || d.model_type === selectedModelType) &&
            (selectedPk === 'All' || d.pk === selectedPk)
        );
    }, [data, selectedTicker, selectedModelType, selectedPk]);

    const aggregatedData = useMemo(() => {
        if (filteredData.length === 0) return [];
        const grouped = filteredData.reduce((acc, curr) => {
            const key = curr.ticker;
            if (!acc[key]) {
                acc[key] = { ticker: key, count: 0, accuracy: 0, precision: 0, recall: 0, total_pnl: 0 };
            }
            acc[key].count += 1;
            acc[key].accuracy += curr.accuracy || 0;
            acc[key].precision += curr.precision || 0;
            acc[key].recall += curr.recall || 0;
            acc[key].total_pnl += curr.total_pnl || 0;  // Aggregate total_pnl
            return acc;
        }, {});

        return Object.values(grouped).map(g => ({
            ...g,
            total_pnl: g.total_pnl / g.count,  // Average total_pnl
            accuracy: g.accuracy / g.count,
            precision: g.precision / g.count,
            recall: g.recall / g.count,
        }));
    }, [filteredData]);

    // Sort filtered data by date
    const sortedFilteredData = useMemo(() => {
        return [...filteredData].sort((a, b) => new Date(a.test_date) - new Date(b.test_date));
    }, [filteredData]);

    const overallPerformance = useMemo(() => {
        if (aggregatedData.length === 0) {
            return { total_pnl: 0, accuracy: 0, precision: 0, recall: 0 };
        }
        const total = aggregatedData.reduce((acc, curr) => {
            acc.total_pnl += curr.total_pnl * curr.count;  // Total for overall performance
            acc.accuracy += curr.accuracy * curr.count;
            acc.precision += curr.precision * curr.count;
            acc.recall += curr.recall * curr.count;
            acc.count += curr.count;
            return acc;
        }, { total_pnl: 0, accuracy: 0, precision: 0, recall: 0, count: 0 });

        return {
            total_pnl: total.total_pnl / total.count || 0,  // Average total_pnl
            accuracy: total.accuracy / total.count || 0,
            precision: total.precision / total.count || 0,
            recall: total.recall / total.count || 0,
        };
    }, [aggregatedData]);

    return (
        <Box sx={{ p: 4 }}>
            <Box sx={{ display: 'flex', gap: 2, mb: 4 }}>
                {/* Ticker Dropdown */}
                <FormControl sx={{ minWidth: 120 }}>
                    <InputLabel id="ticker-label">Ticker</InputLabel>
                    <Select
                        labelId="ticker-label"
                        value={selectedTicker}
                        onChange={(e) => setSelectedTicker(e.target.value)}
                        label="Ticker"
                    >
                        {tickers.map(ticker => (
                            <MenuItem key={ticker} value={ticker}>{ticker}</MenuItem>
                        ))}
                    </Select>
                </FormControl>

                {/* Metric Dropdown */}
                <FormControl sx={{ minWidth: 120 }}>
                    <InputLabel id="metric-label">Metric</InputLabel>
                    <Select
                        labelId="metric-label"
                        value={selectedMetric}
                        onChange={(e) => setSelectedMetric(e.target.value)}
                        label="Metric"
                    >
                        <MenuItem value="total_pnl">Total PnL</MenuItem>
                        <MenuItem value="accuracy">Accuracy</MenuItem>
                        <MenuItem value="precision">Precision</MenuItem>
                        <MenuItem value="recall">Recall</MenuItem>
                    </Select>
                </FormControl>

                {/* Model Type Dropdown */}
                <FormControl sx={{ minWidth: 120 }}>
                    <InputLabel id="model-type-label">Model Type</InputLabel>
                    <Select
                        labelId="model-type-label"
                        value={selectedModelType}
                        onChange={(e) => setSelectedModelType(e.target.value)}
                        label="Model Type"
                    >
                        {modelTypes.map(type => (
                            <MenuItem key={type} value={type}>{type}</MenuItem>
                        ))}
                    </Select>
                </FormControl>

                {/* PK Dropdown */}
                <FormControl sx={{ minWidth: 120 }}>
                    <InputLabel id="pk-label">Simulated Run</InputLabel>
                    <Select
                        labelId="pk-label"
                        value={selectedPk}
                        onChange={(e) => setSelectedPk(e.target.value)}
                        label="PK"
                    >
                        {pks.map(pk => (
                            <MenuItem key={pk} value={pk}>{pk}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </Box>

            <Box sx={{ display: 'grid', gridTemplateColumns: 'repeat(3, 1fr)', gap: 2, mb: 4 }}>
                <Card>
                    <CardContent>
                        <Typography variant="h6" gutterBottom>Overall Performance</Typography>
                        <Typography>Total PnL: {overallPerformance.total_pnl.toFixed(4)}</Typography>
                        <Typography>Accuracy: {overallPerformance.accuracy.toFixed(4)}</Typography>
                        <Typography>Precision: {overallPerformance.precision.toFixed(4)}</Typography>
                        <Typography>Recall: {overallPerformance.recall.toFixed(4)}</Typography>
                    </CardContent>
                </Card>

                <Card>
                    <CardContent>
                        <Typography variant="h6" gutterBottom>Performance by Ticker</Typography>
                        <ResponsiveContainer width="100%" height={300}>
                            <BarChart data={aggregatedData}>
                                <CartesianGrid strokeDasharray="3 3" />
                                <XAxis dataKey="ticker" />
                                <YAxis />
                                <Tooltip />
                                <Bar dataKey={selectedMetric} fill="#8884d8" />
                            </BarChart>
                        </ResponsiveContainer>
                    </CardContent>
                </Card>

                <Card>
                    <CardContent>
                        <Typography variant="h6" gutterBottom>Performance Over Time</Typography>
                        <ResponsiveContainer width="100%" height={300}>
                            <LineChart data={sortedFilteredData}>
                                <CartesianGrid strokeDasharray="3 3" />
                                <XAxis
                                    dataKey="test_date"
                                    type="category"
                                    tickFormatter={(value) => new Date(value).toLocaleDateString()}
                                    interval="preserveStartEnd"
                                />
                                <YAxis />
                                <Tooltip
                                    labelFormatter={(value) => new Date(value).toLocaleDateString()}
                                />
                                <Line type="monotone" dataKey={selectedMetric} stroke="#8884d8" />
                            </LineChart>
                        </ResponsiveContainer>
                    </CardContent>
                </Card>
            </Box>

            <Card>
                <CardContent>
                    <Typography variant="h6" gutterBottom>Detailed Results</Typography>
                    <TableContainer component={Paper}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Ticker</TableCell>
                                    <TableCell>Test Date</TableCell>
                                    <TableCell>Total PnL</TableCell>
                                    <TableCell>Accuracy</TableCell>
                                    <TableCell>Precision</TableCell>
                                    <TableCell>Recall</TableCell>
                                    <TableCell>Features</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {filteredData.map((row, index) => (
                                    <TableRow key={index}>
                                        <TableCell>{row.ticker}</TableCell>
                                        <TableCell>{row.test_date}</TableCell>
                                        <TableCell>{row.total_pnl?.toFixed(4)}</TableCell>
                                        <TableCell>{row.accuracy?.toFixed(4)}</TableCell>
                                        <TableCell>{row.precision?.toFixed(4)}</TableCell>
                                        <TableCell>{row.recall?.toFixed(4)}</TableCell>
                                        <TableCell>{row.calculated_features?.join(', ')}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </CardContent>
            </Card>
        </Box>
    );
};

export default SimulationResultsDashboard;