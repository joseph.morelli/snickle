import React, { useState } from 'react';
import axios from 'axios';
import { TextField, Button, Grid } from '@mui/material';

function Trade({ onActionComplete }) {
  const [signal, setSignal] = useState('');
  const [ticker, setTicker] = useState('');
  const [qty, setQty] = useState('');
  const [predictFk, setPredictFk] = useState(''); // New state for predict_fk
  const [response, setResponse] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setResponse(null); // Clear previous response
    try {
      const response = await axios.post('http://localhost:8080/trader/make-trade', {
        signal,
        ticker,
        qty,
        predict_fk: predictFk, // Include predict_fk in the POST request
      });
      setResponse(response.data);
      onActionComplete();
    } catch (error) {
      console.error(error);
      setResponse({ error: error.message });
    }
  };

  return (
    <div>
      <h2>Trade</h2>
      <Grid container spacing={1}>
        <Grid item sm={2.5}>
          <TextField
            id="outlined-signal"
            label="Signal"
            variant="outlined"
            value={signal}
            onChange={(e) => setSignal(e.target.value)}
          />
        </Grid>
        <Grid item sm={2.5}>
          <TextField
            id="outlined-ticker"
            label="Ticker"
            variant="outlined"
            value={ticker}
            onChange={(e) => setTicker(e.target.value)}
          />
        </Grid>
        <Grid item sm={2.5}>
          <TextField
            id="outlined-qty"
            label="Quantity"
            variant="outlined"
            value={qty}
            type="number"
            onChange={(e) => setQty(e.target.value)}
          />
        </Grid>
        <Grid item sm={2.5}>
          <TextField
            id="outlined-predict-fk"
            label="Predict FK"
            variant="outlined"
            value={predictFk}
            onChange={(e) => setPredictFk(e.target.value)}
          />
        </Grid>
      </Grid>
      <br />
      <Button variant="contained" onClick={handleSubmit} color="inherit">
        Trade
      </Button>
      {response && <pre>{JSON.stringify(response, null, 2)}</pre>}
    </div>
  );
}

export default Trade;
