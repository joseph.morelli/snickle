import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Container, Typography } from '@mui/material';
import HomePage from './components/HomePage';
import Train from './components/Train';
import Predict from './components/Predict';
import Trade from './components/Trade';
import Layout from './components/Layout';
import SimulationResultsDashboard from './components/SimulationResultsDashboard';

const Testing = () => (
  <Container>
    <Typography variant="h4" gutterBottom>Testing Interface</Typography>
    <Train />
    <Predict />
    <Trade />
  </Container>
);

const SimulationDashboard = () => (
  <Container>
    <Typography variant="h4" gutterBottom>Simulation Dashboard</Typography>
    <SimulationResultsDashboard />
  </Container>
);

const App = () => {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/testing" element={<Testing />} />
          <Route path="/simulations" element={<SimulationDashboard />} />
        </Routes>
      </Layout>
    </Router>
  );
};

export default App;
