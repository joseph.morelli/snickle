# Snickle

# DOCUMENTATION

## Up and running


## Trading algo

### Configs
`trading_algo/configs/configs.yaml`
- Main snickle configuration. Includes necessary setup, trading details, orchestration, simulation, and prod model

`trading_algo/configs/model/rf_config.yaml`
- Model-specific configurations. Includes features, required minimum records for a prod prediction, and label specs

### Database schema
#### Models
- **predict_events**
  - Columns:
    - `pk`
    - `orchestrator_fk`
    - `train_fk`
    - `ticker`
    - `prediction`
    - `strength`
    - `execution_time`
    - `date`
    - `meta`

- **train_events**
  - Columns:
    - `pk`
    - `orchestrator_fk`
    - `date`
    - `execution_time`
    - `meta`

#### Orchestrator
- **tickers**
  - Columns:
    - `active`
    - `ticker`

#### Transactions
- **orchestrator_events**
  - Columns:
    - `pk`
    - `date`
    - `execution_time`
    - `type`

- **transaction_events**
  - Columns:
    - `pk`
    - `date`
    - `execution_time`
    - `predict_fk`
    - `orchestrator_fk`
    - `ticker`
    - `qty`
    - `signal`
    - `meta`

### Main
`trading_algo/main.py`
- This file sets up the Flask application, configures routes for model training, prediction, trading, and integrates with a scheduler for running periodic tasks. It serves as the entry point for the trading system's API.

- Key Components:
  - **Flask Application**: Initializes the Flask app and configures CORS (Cross-Origin Resource Sharing).
  - **Configuration Loading**: Loads configuration settings from a YAML file (`configs.yaml`), which includes API keys, paths, database connections, and trading parameters.
  - **Database Connection**: Establishes a connection to a PostgreSQL database using SQLAlchemy. It creates necessary schemas (models, transactions) if they do not already exist.
  - **Scheduler**: Uses APScheduler to schedule tasks like data fetching or model predictions.
  - **Trading Executor**: Instantiates the `TradeExecutor` for executing trades based on signals.
  - **Logging**: Configures logging for tracking the application's actions and errors.
  - **Argument Parsing**: Allows running the app in either debug or production mode (using Waitress) through command-line arguments.
  
- Key Routes:
  - `/ (GET)`: A simple test route to check if the API is running. Returns a JSON response with a success message.
  - `/event_data (GET)`: Allows downloading of event data files. This route retrieves the path from the configuration and sends the file to the client.
  - `/trader/data (GET)`: Retrieves trade data from the specified database table. Accepts a JSON payload specifying the table name and returns the requested data.
  - `/model/train (POST)`: Handles model training requests. It accepts a JSON payload with the stock ticker, start date, and end date for the training period. The route processes the data, trains the model, caches the model, and logs the event.
  - `/model/predict (POST)`: Handles prediction requests. It uses the cached model if available or loads it from storage. The route processes the input data, makes predictions, logs the event, and returns the results.
  - `/trader/trade (POST)`: Executes trades based on the received trading signal (buy/sell), ticker symbol, and quantity. It uses the `TradeExecutor` to carry out the trade and logs the transaction.
  - `/scheduler/start-prediction-job (POST)`: (Implementation in progress) Intended to trigger the scheduler to start prediction jobs.
  - `/db/schema-info (GET)`: Returns schema information of the PostgreSQL database, including columns for each table.
  - `/db/table-output (GET)`: Retrieves all data from a specified table in the PostgreSQL database based on the schema and table name provided in the request payload.
  - `/report/model-simulation-metrics (GET)`: Retrieves simulation metrics generated from model runs, including calculated features and percentiles.
  - `/report/transaction-metrics (GET)`: Retrieves transaction metrics from orchestrated runs, used for reporting day-level metrics based on a specified date range.
  - `/trader/make-trade (POST)`: Executes a trade based on the received trade signal, ticker, and quantity. It uses the `TradeExecutor` to perform the trade.
  - `/trader/cancel-open (POST)`: Cancels open orders for a given ticker, with the option to specify the types of orders to cancel (e.g., limit, stop).
  - `/orchestrator/tickers (POST)`: Updates the active tickers table with new ticker data.
  - `/orchestrator/tickers (GET)`: Retrieves the status of all tickers, including whether they are active or not.
  - `/orchestrator/active-tickers (GET)`: Retrieves a list of active tickers.

### Trade Orcehstrator
`trading_algo/trade_orcehstrator.py`
- abcdefg

### Simulation
`trading_algo/simulation/`
- Allows for testing of different trading models. First run `get_raw_stock_data.py`, then `run_model_simulation.py`.
- Configuration pulled from both `configs.yaml` and the specific model config (e.g. `rf_config.yaml`)
- Simulation process:
  1) Pull in raw stock data
  2) Create sliding train and test windows
  3) Iterate over those windows, shifting forward in time by one day:
      - Run predictions for every row
      - Calculate labels
      - Simulate model run for test window, which typically represents a single day
      - Calculate success metrics (accuracy, precision, recall)
      - Calculate simulated PnL
      - Record all metrics to a parquet file

### Data Pull
`trading_algo/lib/data_pull.py`
- abcdefg

### Model Strategy
`trading_algo/lib/model_strategy.py`
- abcdefg

### Reporting
`trading_algo/lib/reporting.py`
- abcdefg

### Trade Executor
`trading_algo/lib/trade_executor.py`
- abcdefg

### Trading Model
`trading_algo/lib/trading_model.py`
- abcdefg

## Trading app



## Secret Manager Setup

1. Create a secret named ALPACA_APIS_SECRETS in Google Cloud Secret Manager:
The JSON structure for ALPACA_API_SECRETS should be as follows:

```json
{"API_KEY":"your_api_key_here",
"API_SECRET":"your_api_secret_here",
"PAPER_ENDPOINT":"https://paper-api.alpaca.markets"}
```
If another secret name is used, modify the configs.yaml file
```yaml
  alpaca: # trading
    secret_name: "your_secret_name_here"
```




---
# TODO: Fold into above sections

### Data processing
- Leverages calculated_features and feature_params to build the df
- calculated_features determines which feature_params to use to create the dataset (derived dataset and features)
- Final dataset also includes Open, High, Low, Close, Adj Close, Volume (core metrics from feature_minute_date/get_data_in_chunks)

*data_pull*
- fetch_minute_data: check if start=end. If so, pull start-1 through end+1 to ensure enough data for features
- get_data_in_chunks: entry point, calls fetch_minute_data.
  - if serve, no chunking needed. uses input start_date and end_date
  - if train, break up request to reduce API load




### Reporting
- Calculate from raw events on report generation - net profit, total trades, total qty, avg profit per trade, 

### trading_model
- Instantiate with a trained model if it exists
- Handles data pull for train and predict

*train_model*
- Train a model. Training set created from data_preprocessor
- Features determined by contents of config calculated_fields and feature_params
- Labels (0,1) where 1 represents a price increase from 5 records ago (currently minute level data)
- Calculate volatility_threshold
- Train model
- Save model with detailed info, so it can be pulled and used for prediction
- Update instance variables

*predict*
- Simple prediction on input data
- Takes pred_record_req as an input that represents the minimum amount of data (minute-level records) needed
    to calculate all the lag features

### trade_executor
The TradeExecutor class provides a structured way to interact with the Alpaca trading API for executing
    automated trading strategies. It handles the setup of API credentials, market status checks, order
    execution, and transaction logging. This class is designed to be configurable through portions of the
    config.yaml, allowing for flexible adjustment of trading parameters and settings.

*cancel_open_orders*
- This method cancels open orders for a specified stock ticker and optionally filters by order types such
as "limit" or "stop". It first retrieves the current open orders for the ticker and then iterates through
them, canceling those that match the specified criteria. This function is useful for managing and
clearing out stale or unwanted orders before placing new ones.

*execute_trade*
- The execute_trade method handles the core logic for executing a trade. It requires a trade signal ("buy" or
"sell"), a ticker symbol, and the quantity of stock to trade. The method first updates the order, market,
and position statuses, and then checks the following conditions before executing the trade:

  - Existing Open Orders: If there are existing open orders for the ticker, the trade is aborted to avoid
  conflicts or duplication.
  - Market Status: It verifies that the market is open. If the market is closed, the trade cannot proceed.
  - Order Execution: Depending on the signal ("buy" or "sell"), the method submits a corresponding order.
  For buy orders, it ensures sufficient buying power; for sell orders, it verifies that enough shares are owned.
  - After these checks, the method waits for the order to be filled and logs the transaction details.

